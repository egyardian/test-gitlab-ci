FROM node:12-alpine

RUN apk update && apk upgrade && \
    apk add --no-cache bash git tzdata vim nano

ENV TZ Asia/Jakarta
    
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install

COPY . .

# ENV APP_SETTINGS_FILE_PATH '/usr/src/app/config/appSettings.json'

EXPOSE 4001
CMD ["node", "./src/app.js"]