const merchant_imgProperties = {
  // id: {
  //   type: 'string'
  // },
  // merchant_img_name: {
  //   type: 'string'
  // },
  // description: {
  //   type: 'string',
  //   nullable: true
  // }
};

const tags = ['merchant_img'];

const paramsJsonSchema = {
  type: 'object',
  properties: {
    id: {
      type: 'string'
    }
  },
  required: ['id']
};

// const queryStringJsonSchema = {
//   type: 'object',
//   properties: {
//     filter: {
//       type: 'string'
//     }
//   },
//   // required: ['filter']
// };

const bodyCreateJsonSchema = {
  type: 'object',
  properties: merchant_imgProperties,
  // required: ['merchant_img_name', 'description']
};

const bodyUpdateJsonSchema = {
  type: 'object',
  properties: merchant_imgProperties
};

const getAllSchema = {
  tags,
  // response: {
  //   200: {
  //     properties: merchant_imgProperties
  //   }
  // }
};

const getOneSchema = {
  tags,
  params: paramsJsonSchema,
  //querystring: queryStringJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: merchant_imgProperties
  //   }
  // }
};

const createSchema = {
  tags,
  body: bodyCreateJsonSchema,
  // response: {
  //   201: {
  //     type: 'object',
  //     properties: merchant_imgProperties
  //   }
  // }
};

const updateSchema = {
  tags,
  // params: paramsJsonSchema,
  // body: bodyUpdateJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: merchant_imgProperties
  //   }
  // }
};

const deleteSchema = {
  tags,
  //params: paramsJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: merchant_imgProperties
  //   }
  // }
};

module.exports = {
  getAllSchema,
  getOneSchema,
  createSchema,
  updateSchema,
  deleteSchema
};