const merchant_imgService = require('../../../services/merchant_img');
const {
  createSchema,
  getAllSchema,
  getOneSchema,
  updateSchema,
  deleteSchema
} = require('./schemas');

async function merchant_imgRoutes(app, options) {

  // get all  
  app.route({
    method: 'GET',
    url: '/',
    schema: getAllSchema,
    handler: merchant_imgService.getAll

  });

  // get One
  app.route({
    method: 'GET',
    url: '/:id',
    schema: getOneSchema,
    handler: merchant_imgService.getById

  });

  //create merchant_img
  app.route({
    method: 'POST',
    url: '/',
    schema: createSchema,
    handler: merchant_imgService.create

  });


  app.route({

    method: 'PUT',
    url: '/:id',
    schema: updateSchema,
    handler: merchant_imgService.update

  });

  app.route({

    method: 'DELETE',
    url: '/',
    schema: deleteSchema,
    handler: merchant_imgService.destroy

  });

};

module.exports = merchant_imgRoutes;