const courirProperty = {
  // id: {
  //   type: 'string'
  // },
  // category_name: {
  //   type: 'string'
  // },
  // description: {
  //   type: 'string',
  //   nullable: true
  // }
};

const tags = ['courir'];

const paramsJsonSchema = {
  type: 'object',
  properties: {
    id: {
      type: 'string'
    }
  },
  required: ['id']
};

// const queryStringJsonSchema = {
//   type: 'object',
//   properties: {
//     filter: {
//       type: 'string'
//     }
//   },
//   // required: ['filter']
// };

const bodyCreateJsonSchema = {
  type: 'object',
  properties: courirProperty,
  // required: ['category_name', 'description']
};

const bodyUpdateJsonSchema = {
  type: 'object',
  properties: courirProperty
};

const getAllSchema = {
  tags,
  // response: {
  //   200: {
  //     properties: courirProperty
  //   }
  // }
};

const getOneSchema = {
  tags,
  params: paramsJsonSchema,
  //querystring: queryStringJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: courirProperty
  //   }
  // }
};

const createSchema = {
  tags,
  body: bodyCreateJsonSchema,
  // response: {
  //   201: {
  //     type: 'object',
  //     properties: courirProperty
  //   }
  // }
};

const updateSchema = {
  tags,
  params: paramsJsonSchema,
  // body: bodyUpdateJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: courirProperty
  //   }
  // }
};

const deleteSchema = {
  tags,
  //params: paramsJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: courirProperty
  //   }
  // }
};

module.exports = {
  getAllSchema,
  getOneSchema,
  createSchema,
  updateSchema,
  deleteSchema
};