const courirService = require('../../../services/courir');
const {
  createSchema,
  getAllSchema,
  getOneSchema,
  updateSchema,
  deleteSchema
} = require('./schemas');

async function courirRoutes(app, options) {

  // get all 
  app.route({
    method: 'GET',
    url: '/',
    schema: getAllSchema,
    handler: courirService.getAll

  });

  // get One
  app.route({
    method: 'GET',
    url: '/:id',
    schema: getOneSchema,
    handler: courirService.getById

  });


  app.route({
    method: 'POST',
    url: '/',
    schema: createSchema,
    handler: courirService.create

  });


  app.route({

    method: 'PUT',
    url: '/:id',
    schema: updateSchema,
    handler: courirService.update

  });

  app.route({
    method: 'DELETE',
    url: '/',
    schema: deleteSchema,
    handler: courirService.destroy

  });

};

module.exports = courirRoutes;