const contentService = require("../../../services/content");

async function contentRoutes(app, options) {
  // get all
  app.route({
    method: "GET",
    url: "/",
    handler: contentService.getAll,
  });

  // get One
  app.route({
    method: "GET",
    url: "/:id",
    handler: contentService.getById,
  });

  //create content
  app.route({
    method: "POST",
    url: "/",
    handler: contentService.create,
  });

  app.route({
    method: "PUT",
    url: "/:id",
    handler: contentService.update,
  });

  app.route({
    method: "DELETE",
    url: "/:id",
    handler: contentService.destroy,
  });
}

module.exports = contentRoutes;
