const productsService = require("../../../services/products");

async function productsRoutes(app, options) {
  // get all
  app.route({
    method: "GET",
    url: "/",
    handler: productsService.getAll,
  });

  // get One
  app.route({
    method: "GET",
    url: "/:id",
    handler: productsService.getById,
  });

  //get product by merchant id
  app.route({
    method: "GET",
    url: "/merchant/:id",
    handler: productsService.getByMerchantId,
  });

  //create products
  app.route({
    method: "POST",
    url: "/",
    handler: productsService.create,
  });

  app.route({
    method: "PUT",
    url: "/:id",
    handler: productsService.update,
  });

  app.route({
    method: "DELETE",
    url: "/:id",
    handler: productsService.destroy,
  });
}

module.exports = productsRoutes;
