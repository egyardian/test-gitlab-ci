const bannerService = require("../../../services/banner");

async function bannerRoutes(app, options) {
  // get all
  app.route({
    method: "GET",
    url: "/",
    handler: bannerService.getAll,
  });

  // get all by validation
  app.route({
    method: "GET",
    url: "/validation/",
    handler: bannerService.getAllByValidation,
  });

  // get One
  app.route({
    method: "GET",
    url: "/:id",
    handler: bannerService.getById,
  });

  // create new banner
  app.route({
    method: "POST",
    url: "/",
    handler: bannerService.create,
  });

  app.route({
    method: "PUT",
    url: "/:id",
    handler: bannerService.update,
  });

  app.route({
    method: "PUT",
    url: "/status",
    handler: bannerService.updateStatus,
  });

  app.route({
    method: "PUT",
    url: "/sequence",
    handler: bannerService.sequence,
  });

  app.route({
    method: "DELETE",
    url: "/:id",
    handler: bannerService.destroy,
  });
}

module.exports = bannerRoutes;
