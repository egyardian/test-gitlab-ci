const ordersProperties = {
  // id: {
  //   type: 'string'
  // },
  // orders_name: {
  //   type: 'string'
  // },
  // description: {
  //   type: 'string',
  //   nullable: true
  // }
};

const tags = ['orders'];

const paramsJsonSchema = {
  type: 'object',
  properties: {
    id: {
      type: 'string'
    }
  },
  required: ['id']
};

// const queryStringJsonSchema = {
//   type: 'object',
//   properties: {
//     filter: {
//       type: 'string'
//     }
//   },
//   // required: ['filter']
// };

const bodyCreateJsonSchema = {
  type: 'object',
  properties: ordersProperties,
  // required: ['orders_name', 'description']
};

const bodyUpdateJsonSchema = {
  type: 'object',
  properties: ordersProperties
};

const getAllSchema = {
  tags,
  // response: {
  //   200: {
  //     properties: ordersProperties
  //   }
  // }
};

const getOneSchema = {
  tags,
  params: paramsJsonSchema,
  //querystring: queryStringJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: ordersProperties
  //   }
  // }
};

const createSchema = {
  tags,
  body: bodyCreateJsonSchema,
  // response: {
  //   201: {
  //     type: 'object',
  //     properties: ordersProperties
  //   }
  // }
};

const updateSchema = {
  tags,
  // params: paramsJsonSchema,
  // body: bodyUpdateJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: ordersProperties
  //   }
  // }
};

const deleteSchema = {
  tags,
  //params: paramsJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: ordersProperties
  //   }
  // }
};

module.exports = {
  getAllSchema,
  getOneSchema,
  createSchema,
  updateSchema,
  deleteSchema
};