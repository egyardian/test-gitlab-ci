const ordersService = require('../../../services/orders');
const {
  createSchema,
  getAllSchema,
  getOneSchema,
  updateSchema,
  deleteSchema
} = require('./schemas');

async function ordersRoutes(app, options) {

  // get all  
  app.route({
    method: 'GET',
    url: '/',
    schema: getAllSchema,
    handler: ordersService.getAll

  });

  // get One
  app.route({
    method: 'GET',
    url: '/:id',
    schema: getOneSchema,
    handler: ordersService.getById

  });

  //create orders
  app.route({
    method: 'POST',
    url: '/',
    schema: createSchema,
    handler: ordersService.create

  });


  app.route({

    method: 'PUT',
    url: '/:id',
    schema: updateSchema,
    handler: ordersService.update

  });

  app.route({

    method: 'DELETE',
    url: '/',
    schema: deleteSchema,
    handler: ordersService.destroy

  });

};

module.exports = ordersRoutes;