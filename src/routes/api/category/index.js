const categoryService = require("../../../services/category");

async function categoryRoutes(app, options) {
  // get all
  app.route({
    method: "GET",
    url: "/",
    handler: categoryService.getAll,
  });

  // get One
  app.route({
    method: "GET",
    url: "/:id",
    handler: categoryService.getById,
  });

  //create category
  app.route({
    method: "POST",
    url: "/",
    handler: categoryService.create,
  });

  app.route({
    method: "PUT",
    url: "/:id",
    handler: categoryService.update,
  });

  app.route({
    method: "PUT",
    url: "/status",
    handler: categoryService.updateStatus,
  });

  app.route({
    method: "DELETE",
    url: "/:id",
    handler: categoryService.destroy,
  });

  app.route({
    method: "GET",
    url: "/custom",
    handler: categoryService.getCustom,
  });
}

module.exports = categoryRoutes;
