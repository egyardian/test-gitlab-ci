const imagesService = require('../../../services/images');
const {
  createSchema,
  getAllSchema,
  getOneSchema,
  updateSchema,
  deleteSchema
} = require('./schemas');

async function imagesRoutes(app, options) {

  // get all  
  app.route({
    method: 'GET',
    url: '/',
    schema: getAllSchema,
    handler: imagesService.getAll

  });

  // get One
  app.route({
    method: 'GET',
    url: '/:id',
    schema: getOneSchema,
    handler: imagesService.getById

  });

  //create images
  app.route({
    method: 'POST',
    url: '/',
    schema: createSchema,
    handler: imagesService.create

  });


  app.route({

    method: 'PUT',
    url: '/:id',
    schema: updateSchema,
    handler: imagesService.update

  });

  app.route({

    method: 'DELETE',
    url: '/',
    schema: deleteSchema,
    handler: imagesService.destroy

  });

};

module.exports = imagesRoutes;