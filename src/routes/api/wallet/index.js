const walletService = require("../../../services/wallet");

async function walletRoutes(app, options) {
  // get all
  app.route({
    method: "GET",
    url: "/:id",
    handler: walletService.getAll,
  });

  // create new wallet
  app.route({
    method: "POST",
    url: "/",
    handler: walletService.create,
  });

  app.route({
    method: "PUT",
    url: "/:id",
    handler: walletService.update,
  });
}

module.exports = walletRoutes;
