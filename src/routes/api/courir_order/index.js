const courir_orderService = require('../../../services/courir_order');
const {
  createSchema,
  getAllSchema,
  getOneSchema,
  updateSchema,
  deleteSchema
} = require('./schemas');

async function courir_orderRoutes(app, options) {

  // get all 
  app.route({
    method: 'GET',
    url: '/',
    schema: getAllSchema,
    handler: courir_orderService.getAll

  });

  // get One
  app.route({
    method: 'GET',
    url: '/:id',
    schema: getOneSchema,
    handler: courir_orderService.getById

  });


  app.route({
    method: 'POST',
    url: '/',
    schema: createSchema,
    handler: courir_orderService.create

  });


  app.route({

    method: 'PUT',
    url: '/:id',
    schema: updateSchema,
    handler: courir_orderService.update

  });

  app.route({
    method: 'DELETE',
    url: '/',
    schema: deleteSchema,
    handler: courir_orderService.destroy

  });

};

module.exports = courir_orderRoutes;