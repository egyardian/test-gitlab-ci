const addressService = require("../../../services/address");

async function addressRoutes(app, options) {
  // get all
  app.route({
    method: "GET",
    url: "/",
    handler: addressService.getAll,
  });

  // get One
  app.route({
    method: "GET",
    url: "/:id",
    handler: addressService.getById,
  });

  app.route({
    method: "POST",
    url: "/",
    handler: addressService.create,
  });

  app.route({
    method: "PUT",
    url: "/",
    handler: addressService.update,
  });

  app.route({
    method: "DELETE",
    url: "/:id",
    handler: addressService.destroy,
  });
}

module.exports = addressRoutes;
