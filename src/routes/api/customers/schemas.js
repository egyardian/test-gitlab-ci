const customersProperties = {
  // id: {
  //   type: 'string'
  // },
  // customers_name: {
  //   type: 'string'
  // },
  // description: {
  //   type: 'string',
  //   nullable: true
  // }
};

const tags = ['customers'];

const paramsJsonSchema = {
  type: 'object',
  properties: {
    id: {
      type: 'string'
    }
  },
  required: ['id']
};

// const queryStringJsonSchema = {
//   type: 'object',
//   properties: {
//     filter: {
//       type: 'string'
//     }
//   },
//   // required: ['filter']
// };

const bodyCreateJsonSchema = {
  type: 'object',
  properties: customersProperties,
  // required: ['customers_name', 'description']
};

const bodyUpdateJsonSchema = {
  type: 'object',
  properties: customersProperties
};

const getAllSchema = {
  tags,
  // response: {
  //   200: {
  //     properties: customersProperties
  //   }
  // }
};

const getOneSchema = {
  tags,
  params: paramsJsonSchema,
  //querystring: queryStringJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: customersProperties
  //   }
  // }
};

const createSchema = {
  tags,
  body: bodyCreateJsonSchema,
  // response: {
  //   201: {
  //     type: 'object',
  //     properties: customersProperties
  //   }
  // }
};

const updateSchema = {
  tags,
  // params: paramsJsonSchema,
  // body: bodyUpdateJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: customersProperties
  //   }
  // }
};

const deleteSchema = {
  tags,
  //params: paramsJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: customersProperties
  //   }
  // }
};

module.exports = {
  getAllSchema,
  getOneSchema,
  createSchema,
  updateSchema,
  deleteSchema
};