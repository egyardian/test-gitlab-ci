const customersService = require('../../../services/customers');
const {
  createSchema,
  getAllSchema,
  getOneSchema,
  updateSchema,
  deleteSchema
} = require('./schemas');

async function customersRoutes(app, options) {

  // get all  
  app.route({
    method: 'GET',
    url: '/',
    schema: getAllSchema,
    handler: customersService.getAll

  });

  // get One
  app.route({
    method: 'GET',
    url: '/:id',
    schema: getOneSchema,
    handler: customersService.getById

  });

  //create customers
  app.route({
    method: 'POST',
    url: '/',
    schema: createSchema,
    handler: customersService.create

  });


  app.route({

    method: 'PUT',
    url: '/:id',
    schema: updateSchema,
    handler: customersService.update

  });

  app.route({

    method: 'DELETE',
    url: '/',
    schema: deleteSchema,
    handler: customersService.destroy

  });

};

module.exports = customersRoutes;