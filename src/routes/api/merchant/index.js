const merchantService = require("../../../services/merchant");

async function merchantRoutes(app, options) {
  // get all
  app.route({
    method: "GET",
    url: "/",
    handler: merchantService.getAll,
  });

  // get all merchant schedule
  app.route({
    method: "GET",
    url: "/withschedule/",
    handler: merchantService.getAllMerchantWithSchedule,
  });

  // get One
  app.route({
    method: "GET",
    url: "/:id",
    handler: merchantService.getById,
  });

  //create banner
  app.route({
    method: "POST",
    url: "/",
    handler: merchantService.create,
  });

  app.route({
    method: "PUT",
    url: "/:id",
    handler: merchantService.update,
  });

  app.route({
    method: "DELETE",
    url: "/:id",
    handler: merchantService.destroy,
  });
}

module.exports = merchantRoutes;
