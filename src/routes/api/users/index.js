const usersService = require("../../../services/users");
const {
  createSchema,
  getAllSchema,
  getOneSchema,
  updateSchema,
  deleteSchema,
} = require("./schemas");

async function usersRoutes(app, options) {
  // get all
  app.route({
    method: "GET",
    url: "/",
    schema: getAllSchema,
    handler: usersService.getAll,
  });

  // get One
  app.route({
    method: "GET",
    url: "/:id",
    schema: getOneSchema,
    handler: usersService.getById,
  });

  app.route({
    method: "POST",
    url: "/courier",
    schema: createSchema,
    handler: usersService.createuUserCourier,
  });

  app.route({
    method: "PUT",
    url: "/:id",
    schema: updateSchema,
    handler: usersService.update,
  });

  app.route({
    method: "DELETE",
    url: "/",
    schema: deleteSchema,
    handler: usersService.destroy,
  });
}

module.exports = usersRoutes;
