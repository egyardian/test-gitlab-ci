const spotService = require("../../../services/spot");

async function spotRoutes(app, options) {
  // get all
  app.route({
    method: "GET",
    url: "/",
    handler: spotService.getAll,
  });

  // get One
  app.route({
    method: "GET",
    url: "/:id",
    handler: spotService.getById,
  });

  // get by city
  app.route({
    method: "GET",
    url: "/city/:id",
    handler: spotService.getByCity,
  });

  //create spot
  app.route({
    method: "POST",
    url: "/",
    handler: spotService.create,
  });

  app.route({
    method: "PUT",
    url: "/:id",
    handler: spotService.update,
  });

  app.route({
    method: "DELETE",
    url: "/:id",
    handler: spotService.destroy,
  });
}

module.exports = spotRoutes;
