const merchantP_categoryService = require('../../../services/merchantP_category');
const {
  createSchema,
  getAllSchema,
  getOneSchema,
  updateSchema,
  deleteSchema
} = require('./schemas');

async function merchantP_categoryRoutes(app, options) {

  // get all 
  app.route({
    method: 'GET',
    url: '/',
    schema: getAllSchema,
    handler: merchantP_categoryService.getAll

  });

  // get One
  app.route({
    method: 'GET',
    url: '/:id',
    schema: getOneSchema,
    handler: merchantP_categoryService.getById

  });

  //create merchantP_category
  app.route({
    method: 'POST',
    url: '/',
    schema: createSchema,
    handler: merchantP_categoryService.create

  });


  app.route({

    method: 'PUT',
    url: '/:id',
    schema: updateSchema,
    handler: merchantP_categoryService.update

  });

  app.route({

    method: 'DELETE',
    url: '/',
    schema: deleteSchema,
    handler: merchantP_categoryService.destroy

  });

};

module.exports = merchantP_categoryRoutes;