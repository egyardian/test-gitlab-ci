const merchantP_categoryProperty = {
  // id: {
  //   type: 'string'
  // },
  // title: {
  //   type: 'string'
  // },
  // description: {
  //   type: 'string'
  // },
  // action_url: {
  //   type: 'string'
  // },
  // start_date: {
  //   type: 'date'
  // },
  // end_date: {
  //   type: 'date'
  // },
  // image_url: {
  //   type: 'string'
  // },
  // priority: {
  //   type: 'string'
  // },
  // status: {
  //   type: 'boolean'
  // }
};

const tags = ['merchantP_category'];

const paramsJsonSchema = {
  type: 'object',
  properties: {
    id: {
      type: 'string'
    }
  },
  required: ['id']
};

// const queryStringJsonSchema = {
//   type: 'object',
//   properties: {
//     filter: {
//       type: 'string'
//     }
//   },
//   // required: ['filter']
// };

const bodyCreateJsonSchema = {
  type: 'object',
  properties: merchantP_categoryProperty,
  // required: ['category_name', 'description']
};

const bodyUpdateJsonSchema = {
  type: 'object',
  properties: merchantP_categoryProperty
};

const getAllSchema = {
  tags,
  // response: {
  //   200: {
  //     properties: merchantP_categoryProperty
  //   }
  // }
};

const getOneSchema = {
  tags,
  params: paramsJsonSchema,
  //querystring: queryStringJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: merchantP_categoryProperty
  //   }
  // }
};

const createSchema = {
  tags,
  body: bodyCreateJsonSchema,
  // response: {
  //   201: {
  //     type: 'object',
  //     properties: merchantP_categoryProperty
  //   }
  // }
};

const updateSchema = {
  tags,
  params: paramsJsonSchema,
  // body: bodyUpdateJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: merchantP_categoryProperty
  //   }
  // }
};

const deleteSchema = {
  tags,
  //params: paramsJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: merchantP_categoryProperty
  //   }
  // }
};

module.exports = {
  getAllSchema,
  getOneSchema,
  createSchema,
  updateSchema,
  deleteSchema
};