const depositService = require("../../../services/deposit");

async function depositRoutes(app, options) {
  // get all
  app.route({
    method: "GET",
    url: "/",
    handler: depositService.getAll,
  });

  app.route({
    method: "POST",
    url: "/",
    handler: depositService.create,
  });
}

module.exports = depositRoutes;
