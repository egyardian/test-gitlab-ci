const homepage_categoryProperty = {
  // id: {
  //   type: 'string'
  // },
  // category_name: {
  //   type: 'string'
  // },
  // description: {
  //   type: 'string',
  //   nullable: true
  // }
};

const tags = ["homepage_category"];

const paramsJsonSchema = {
  type: "object",
  properties: {
    id: {
      type: "string",
    },
  },
  required: ["id"],
};

// const queryStringJsonSchema = {
//   type: 'object',
//   properties: {
//     filter: {
//       type: 'string'
//     }
//   },
//   // required: ['filter']
// };

const bodyCreateJsonSchema = {
  type: "object",
  properties: homepage_categoryProperty,
  // required: ['category_name', 'description']
};

const bodyUpdateJsonSchema = {
  type: "object",
  properties: homepage_categoryProperty,
};

const getAllSchema = {
  tags,
  // response: {
  //   200: {
  //     properties: homepage_categoryProperty
  //   }
  // }
};

const getOneSchema = {
  tags,
  params: paramsJsonSchema,
  //querystring: queryStringJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: homepage_categoryProperty
  //   }
  // }
};

const createSchema = {
  tags,
  body: bodyCreateJsonSchema,
  // response: {
  //   201: {
  //     type: 'object',
  //     properties: homepage_categoryProperty
  //   }
  // }
};

const updateSchema = {
  tags,
  params: paramsJsonSchema,
  // body: bodyUpdateJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: homepage_categoryProperty
  //   }
  // }
};

const deleteSchema = {
  tags,
  //params: paramsJsonSchema,
  // response: {
  //   200: {
  //     type: 'object',
  //     properties: homepage_categoryProperty
  //   }
  // }
};

module.exports = {
  getAllSchema,
  getOneSchema,
  createSchema,
  updateSchema,
  deleteSchema,
};
