const homepage_categoryService = require("../../../services/homepage_category");
const {
  createSchema,
  getAllSchema,
  getOneSchema,
  updateSchema,
  deleteSchema,
} = require("./schemas");

async function homepage_categoryRoutes(app, options) {
  // get all
  app.route({
    method: "GET",
    url: "/",
    handler: homepage_categoryService.getAll,
  });

  // get One
  app.route({
    method: "GET",
    url: "/:id",
    handler: homepage_categoryService.getById,
  });

  // create new homepage_category
  app.route({
    method: "POST",
    url: "/",
    handler: homepage_categoryService.create,
  });

  app.route({
    method: "PUT",
    url: "/:id",
    handler: homepage_categoryService.update,
  });

  app.route({
    method: "DELETE",
    url: "/",
    handler: homepage_categoryService.destroy,
  });
}

module.exports = homepage_categoryRoutes;
