const scheduleService = require("../../../services/schedule");

async function scheduleRoutes(app, options) {
  // get all
  app.route({
    method: "GET",
    url: "/",
    handler: scheduleService.getAll,
  });

  // get One
  app.route({
    method: "GET",
    url: "/:id",
    handler: scheduleService.getByIdMerchant,
  });

  //create banner
  app.route({
    method: "POST",
    url: "/",
    handler: scheduleService.create,
  });

  app.route({
    method: "PUT",
    url: "/:id",
    handler: scheduleService.update,
  });

  app.route({
    method: "DELETE",
    url: "/:id",
    handler: scheduleService.deleted,
  });
}

module.exports = scheduleRoutes;
