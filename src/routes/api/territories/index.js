const territoriesService = require("../../../services/territories");

async function territoriesRoutes(app, options) {
  // get all
  app.route({
    method: "GET",
    url: "/",
    handler: territoriesService.getAll,
  });

  app.route({
    method: "GET",
    url: "/mastercode/:id",
    handler: territoriesService.getByMst,
  });

  app.route({
    method: "GET",
    url: "/:id",
    handler: territoriesService.getTerritories,
  });

  app.route({
    method: "GET",
    url: "/level/:id",
    handler: territoriesService.getByLevel,
  });

  //city
  app.route({
    method: "GET",
    url: "/city",
    handler: territoriesService.getCity,
  });

  //city
  app.route({
    method: "GET",
    url: "/kecamatan",
    handler: territoriesService.getKec,
  });

  //kelurahan
  app.route({
    method: "GET",
    url: "/kelurahan",
    handler: territoriesService.getKel,
  });
}

module.exports = territoriesRoutes;
