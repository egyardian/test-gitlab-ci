const order_productService = require('../../../services/order_product');
const {
  createSchema,
  getAllSchema,
  getOneSchema,
  updateSchema,
  deleteSchema
} = require('./schemas');

async function order_productRoutes(app, options) {

  // get all  
  app.route({
    method: 'GET',
    url: '/',
    schema: getAllSchema,
    handler: order_productService.getAll

  });

  // get One
  app.route({
    method: 'GET',
    url: '/:id',
    schema: getOneSchema,
    handler: order_productService.getById

  });

  //create order_product
  app.route({
    method: 'POST',
    url: '/',
    schema: createSchema,
    handler: order_productService.create

  });


  app.route({

    method: 'PUT',
    url: '/:id',
    schema: updateSchema,
    handler: order_productService.update

  });

  app.route({

    method: 'DELETE',
    url: '/',
    schema: deleteSchema,
    handler: order_productService.destroy

  });

};

module.exports = order_productRoutes;