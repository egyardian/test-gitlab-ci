const oas = require("fastify-swagger");

const apiRoutes = async (app, options) => {
  app.register(oas, require("../docs"));

  app.register(require("./category"), {
    prefix: "category",
  });

  app.register(require("./banner"), {
    prefix: "banner",
  });

  app.register(require("./courir"), {
    prefix: "courir",
  });

  app.register(require("./merchant"), {
    prefix: "merchant",
  });

  app.register(require("./merchantP_category"), {
    prefix: "merchantP_category",
  });

  app.register(require("./products"), {
    prefix: "products",
  });

  app.register(require("./schedule"), {
    prefix: "schedule",
  });

  app.register(require("./address"), {
    prefix: "address",
  });

  app.register(require("./users"), {
    prefix: "users",
  });

  app.register(require("./courir_order"), {
    prefix: "courir_order",
  });

  app.register(require("./customers"), {
    prefix: "customers",
  });

  app.register(require("./images"), {
    prefix: "images",
  });

  app.register(require("./orders"), {
    prefix: "orders",
  });

  app.register(require("./order_product"), {
    prefix: "order_product",
  });

  app.register(require("./merchant_img"), {
    prefix: "merchant_img",
  });
  app.register(require("./content"), {
    prefix: "content",
  });

  app.register(require("./territories"), {
    prefix: "territories",
  });

  app.register(require("./homepage_category"), {
    prefix: "homepage_category",
  });

  app.register(require("./spot"), {
    prefix: "spot",
  });

  app.register(require("./deposit"), {
    prefix: "deposit",
  });

  app.register(require("./wallet"), {
    prefix: "wallet",
  });

  app.register(require("./userManagement"), {
    prefix: "userManagement",
  });

  app.get("/", async (request, reply) => {
    return {
      hello: "world",
    };
  });
};

module.exports = apiRoutes;
