const userManagementService = require("../../../services/userManagement");

async function userManagementRoutes(app, options) {
  // get all
  app.route({
    method: "GET",
    url: "/",
    handler: userManagementService.getAll,
  });

  // get One
  app.route({
    method: "GET",
    url: "/:id",
    handler: userManagementService.getById,
  });

  //create userManagement
  app.route({
    method: "POST",
    url: "/",
    handler: userManagementService.create,
  });

  app.route({
    method: "PUT",
    url: "/:id",
    handler: userManagementService.update,
  });

  app.route({
    method: "DELETE",
    url: "/:id",
    handler: userManagementService.destroy,
  });
}

module.exports = userManagementRoutes;
