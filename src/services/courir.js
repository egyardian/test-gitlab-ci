let res = require("../response");
let connection = require("../plugins/mysqlConnection");
const util = require("../util");

async function getAll(request, reply) {
  const { limit, offset } = request.query;
  
  let sql = `SELECT
        CONCAT(u.first_name, ' ', u.last_name) AS name,
        c.no_ktp AS no_ktp,
        u.phone_number AS no_telp,
        c.plat_no AS no_plat,
        c.type_kendaraan AS type_kendaraan,
        t1.name AS province,
        t2.name AS city,
        t3.name AS kecamatan,
        t4.name AS kelurahan
      FROM
        courir AS c
        INNER JOIN users AS u ON u.uid = c.user_id
        INNER JOIN addresses AS a ON a.source_id = u.uid
        LEFT JOIN ( SELECT * FROM territories ) AS t1 ON t1.territory_code = a.province
        LEFT JOIN ( SELECT * FROM territories ) AS t2 ON t2.territory_code = a.city
        LEFT JOIN ( SELECT * FROM territories ) AS t3 ON t3.territory_code = a.kecamatan
        LEFT JOIN ( SELECT * FROM territories ) AS t4 ON t4.territory_code = a.kelurahan WHERE`;

  await Object.keys(request.query).forEach((key) => {
    if (!util.isParamsNull(request.query, key)) {
      let coloum = util.camelToSnakeCase(key)
      if (key != 'limit' && key != 'offset') {
        let tables = (coloum == 'courier_name' || coloum == 'no_ktp' || coloum == 'type_kendaraan') ? 'c' : 'a'
        if (coloum == 'courier_name') {
          sql += " " + `u.first_name LIKE "%${request.query[key]}%" OR u.last_name LIKE "%${request.query[key]}%" AND`
        } else {
          sql += " " + tables + `.${coloum} LIKE "%${request.query[key]}%" AND`
        }
      }
    }
  });
  
  // Check if last char in query is 'WHERE' or 'AND' then remove it.
  sql = sql.charAt(sql.length - 1) == 'E' ? sql.substring(0, sql.length - 6) : sql.substring(0, sql.length - 4)
  sql += ` ORDER BY c.id DESC`;

  if (util.isParamsUndifined(request.query, 'limit', 'offset') && !util.isParamsNull(request.query, 'limit', 'offset')) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }

  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );
  if (!data) {
    return res.badData(data, "Data Courir can't be load", reply);
  }

  return res.ok(data, "Data Courir successfully to load", reply);
}

async function getById(request, reply) {
  let id = request.params.id;
  let sql = `SELECT
	  c.*,
    u.*,c.no_ktp,
	dt.detail_address,
	dt.province_name,
	dt.city_name,
	dt.kecamatan_name,
	dt.kelurahan_name 
FROM
	courir AS c inner join users as u on u.uid = c.user_id
	LEFT JOIN (
SELECT
	a.*,
	tp.NAME AS province_name,
	tc.NAME AS city_name,
	tk.NAME AS kecamatan_name,
	tkl.NAME AS kelurahan_name 
FROM
	addresses AS a
	LEFT JOIN territories AS tp ON tp.territory_code = a.province
	LEFT JOIN territories AS tc ON tc.territory_code = a.city
	LEFT JOIN territories AS tk ON tk.territory_code = a.kecamatan
	LEFT JOIN territories AS tkl ON tkl.territory_code = a.kelurahan
ORDER BY
	a.id 
	) AS dt ON dt.source_id = u.uid where c.user_id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(data, "Courir ID can't be found", reply);
  }

  return res.ok(data, "Data Courir successfully to load", reply);
}

async function create(request, reply) {
  const {
    user_id: userId,
    photo_url: photoUrl,
    plat_no: platNo,
    stnk,
    stnk_url: stnkUrl,
    sim,
    sim_url: simUrl,
    type_kendaraan: typeKendaraan,
    no_ktp: noKtp,
    ktp_url: ktpUrl,
    location,
  } = request.body;

  let sql = `INSERT INTO courir ( user_id, photo_url,plat_no,stnk,stnk_url,sim,sim_url,location,type_kendaraan,no_ktp,ktp_url) values( ?,?,?,?,?,?,?,?,?,?,?)`;

  let data = await new Promise((resolve) =>
    connection.query(
      sql,
      [
        userId,
        photoUrl,
        platNo,
        stnk,
        stnkUrl,
        sim,
        simUrl,
        location,
        typeKendaraan,
        noKtp,
        ktpUrl,
      ],
      function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        return rows.affectedRows > 0
          ? resolve({ data: rows, uid: userId })
          : resolve(false);
      }
    )
  );

  if (!data) {
    return res.badData(data, "Data Courir can't add to table", reply);
  }

  return res.ok(data, "Data Courir added to table successfully", reply);
}

async function update(request, reply) {
  const id = request.params.id;
  const now = new Date();
  let queryBuilder = "";
  let arrValue = [];
  const courir = request.body;
  await Object.keys(courir).forEach(function (key) {
    queryBuilder = " " + queryBuilder + key + " = ? ,";
    arrValue.push(courir[key]);
  });
  let sql =
    "UPDATE courir SET " + queryBuilder + "updated_date = ? where id = ?";
  arrValue.push(now);
  arrValue.push(id);
  let data = await new Promise((resolve) =>
    connection.query(sql, arrValue, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Courir can't update to table", reply);
  }

  return res.ok(data, "Data Courir updated to table successfully", reply);
}

async function destroy(request, reply) {
  let id = request.params.id;
  let sql = `DELETE FROM courir WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Courir can't delete from table", reply);
  }

  return res.ok(data, "Data Courir deleted from table successfully", reply);
}
module.exports = {
  getAll,
  getById,
  create,
  update,
  destroy,
};
