let res = require("../response");
let connection = require("../plugins/mysqlConnection");

async function getAll(request, reply) {
  let sql = `SELECT * FROM orders`;
  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      if (rows.length > 0) {
        return resolve(rows);
      } else {
        return resolve([]);
      }
    })
  );
  return res.ok(data, `success load data orders`, reply);
}

async function getById(request, reply) {
  let id = request.params.id;
  let sql = `SELECT * FROM orders WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      if (rows.length > 0) {
        return resolve(rows);
      } else {
        return resolve("data not found");
      }
    })
  );

  return res.ok(data, " success load data orders by Id", reply);
}
//INPUT DATA ARRAY
async function create(request, reply) {
  let id = request.body.id;
  let customers_id = request.body.customers_id;
  let order_date = request.body.order_date;
  let required_date = request.body.required_date;
  let shipped_date = request.body.shipped_date;
  let status = request.body.status;
  let comments = request.body.comments;

  let sql = `insert into orders (id,customers_id, order_date, required_date, shipped_date, status, comments) value (?,?,?,?,?,?,?)`;

  let data = await new Promise((resolve) =>
    connection.query(
      sql,
      [
        id,
        customers_id,
        order_date,
        required_date,
        shipped_date,
        status,
        comments,
      ],
      function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        return rows.affectedRows > 0 ? resolve(true) : resolve(false);
      }
    )
  );

  let msg = [
    data ? "Berhasil menambahkan data!" : "Tidak berhasil menambahkan data!",
  ];
  return res.ok(data, msg, reply);
}

async function update(request, reply) {
  let id = request.params.id;
  let customers_id = request.body.customers_id;
  let order_date = request.body.order_date;
  let required_date = request.body.required_date;
  let shipped_date = request.body.shipped_date;
  let status = request.body.status;
  let comments = request.body.comments;
  let sql = `UPDATE orders SET customers_id =?, order_date =?, required_date =?, shipped_date =?, status =?, comments =?  WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(
      sql,
      [
        customers_id,
        order_date,
        required_date,
        shipped_date,
        status,
        comments,
        id,
      ],
      function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        return rows.affectedRows > 0 ? resolve(true) : resolve(false);
      }
    )
  );

  let msg = data ? "Berhasil mengubah data!" : "Tidak berhasil mengubah data!";
  return res.ok(data, msg, reply);
}

async function destroy(request, reply) {
  let id = request.body.id;
  let sql = `DELETE FROM orders WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  let msg = data
    ? "Berhasil Menghapus data!"
    : "Tidak berhasil Menghapus data!";
  return res.ok(data, msg, reply);
}
module.exports = {
  getAll,
  getById,
  create,
  update,
  destroy,
};
