let res = require("../response");
let connection = require("../plugins/mysqlConnection");

const util = require("../util");

async function getAll(request, reply) {
  const { limit, offset } = request.query;
  let sql = `SELECT 
        c1.*,
        c2.category_name AS parent_name 
    FROM categories AS c1 
        LEFT JOIN categories AS c2 ON c2.id = c1.parent_id 
    ORDER BY c1.id DESC`;

  if (util.isParamsUndifined(request.query, 'limit', 'offset')) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }
  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.badData(data, "Data Category can't be load", reply);
  }

  return res.ok(data, "Data Category successfully to load", reply);
}

async function getById(request, reply) {
  let id = request.params.id;
  let sql = `SELECT * FROM categories WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(data, "Category ID can't be found", reply);
  }

  return res.ok(data, "Data Category successfully to load", reply);
}

async function create(request, reply) {
  const {
    category_name: categoryName,
    description,
    parent_id: parentId,
    image_url: imageUrl,
    status_category: statusCategory,
  } = request.body;
  let sql = `INSERT INTO categories (category_name, description,parent_id,image_url,status_category) values(?, ?, ?,?,?)`;

  let data = await new Promise((resolve) =>
    connection.query(
      sql,
      [categoryName, description, parentId, imageUrl, statusCategory],
      function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        return rows.affectedRows > 0 ? resolve(true) : resolve(false);
      }
    )
  );

  if (!data) {
    return res.badData(data, "Data Category can't add to table", reply);
  }

  return res.ok(data, "Data Category added to table successfully", reply);
}

async function update(request, reply) {
  const id = request.params.id;
  const now = new Date();
  let queryBuilder = "";
  let arrValue = [];
  const category = request.body;
  await Object.keys(category).forEach(function (key) {
    queryBuilder = " " + queryBuilder + key + " = ? ,";
    arrValue.push(category[key]);
  });
  let sql =
    "UPDATE categories SET " + queryBuilder + "updated_date = ? where id = ?";
  arrValue.push(now);
  arrValue.push(id);
  let data = await new Promise((resolve) =>
    connection.query(sql, arrValue, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Category can't update to table", reply);
  }

  return res.ok(data, "Data Category updated to table successfully", reply);
}

async function updateStatus(request, reply) {
  const { id, status_category: statusCategory } = request.body;
  let sql = `UPDATE categories SET status_category=? WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [statusCategory, id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Category can't update to table", reply);
  }

  return res.ok(data, "Data Category updated to table successfully", reply);
}
async function destroy(request, reply) {
  let id = request.params.id;
  let sql = `DELETE FROM categories WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Category can't delete from table", reply);
  }

  return res.ok(data, "Data Category deleted from table successfully", reply);
}

async function getCustom(request, reply) {
  const { limit, offset } = request.query;
  let sql = `SELECT c1.*,c2.category_name as parent_name FROM categories as c1 left join categories as c2 on c2.id = c1.parent_id order by c1.id`;

  if (util.isParamsUndifined(request.query, 'limit', 'offset')) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }
  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      if (rows.length > 0) {
        let array = [];
        rows.forEach((element) => {
          array.push({
            value: element.id,
            label: element.category_name,
          });
        });

        return resolve(array);
      } else {
        return resolve(null);
      }
    })
  );

  if (!data) {
    return res.badData(data, "Data Category can't be load", reply);
  }

  return res.ok(data, "Data Category successfully to load", reply);
}

module.exports = {
  getAll,
  getById,
  create,
  update,
  destroy,
  getCustom,
  updateStatus,
};
