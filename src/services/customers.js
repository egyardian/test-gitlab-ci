let res = require("../response");
let connection = require("../plugins/mysqlConnection");

async function getAll(request, reply) {
  let sql = `SELECT * FROM customers`;
  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      if (rows.length > 0) {
        return resolve(rows);
      } else {
        return resolve([]);
      }
    })
  );
  return res.ok(data, `data category`, reply);
}

async function getById(request, reply) {
  let id = request.params.id;
  let sql = `SELECT * FROM customers WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      if (rows.length > 0) {
        return resolve(rows);
      } else {
        return resolve("data tidak di temukan");
      }
    })
  );

  return res.ok(data, "data category By Id", reply);
}

async function create(request, reply) {
  let id = request.body.id;
  let user_id = request.body.user_id;
  let sql = `INSERT INTO customers (id, user_id) values(?, ?)`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id, user_id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  let msg = data
    ? "Berhasil menambahkan data!"
    : "Tidak berhasil menambahkan data!";
  return res.ok(data, msg, reply);
}

async function update(request, reply) {
  let id = request.params.id;
  let user_id = request.body.user_id;
  let sql = `UPDATE customers SET user_id = ? WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [user_id, id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  let msg = data ? "Berhasil mengubah data!" : "Tidak berhasil mengubah data!";
  return res.ok(data, msg, reply);
}

async function destroy(request, reply) {
  let id = request.body.id;
  let sql = `DELETE FROM customers WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  let msg = data
    ? "Berhasil Menghapus data!"
    : "Tidak berhasil Menghapus data!";
  return res.ok(data, msg, reply);
}
module.exports = {
  getAll,
  getById,
  create,
  update,
  destroy,
};
