let res = require("../response");
let connection = require("../plugins/mysqlConnection");
const util = require("../util");

async function getAll(request, reply) {
  const { content_type, limit, offset } = request.query;

  let sql = `SELECT * FROM contents WHERE`;

  await Object.keys(request.query).forEach((key) => {
    if (!util.isParamsNull(request.query, key)) {
      if (key != "limit" && key != "offset") {
        sql += " " + `content_type = ${request.query[key]} AND`;
      }
    }
  });
  sql =
    sql.charAt(sql.length - 1) == "E"
      ? sql.substring(0, sql.length - 6)
      : sql.substring(0, sql.length - 4);
  sql += ` ORDER BY id DESC`;

  if (
    util.isParamsUndifined(request.query, "limit", "offset") &&
    !util.isParamsNull(request.query, "limit", "offset")
  ) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }

  let data = await new Promise((resolve) =>
    connection.query(sql, [content_type], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.badData(data, "Data Content can't be load", reply);
  }

  return res.ok(data, "Data Content successfully to load", reply);
}

async function getById(request, reply) {
  let id = request.params.id;
  let sql = `SELECT * FROM contents WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(data, "Content ID can't be found", reply);
  }

  return res.ok(data, "Data Content successfully to load", reply);
}

async function create(request, reply) {
  const {
    id,
    content_name,
    status,
    content_desc: contentDesc,
    content_type: contentType,
  } = request.body;
  let sql = `INSERT INTO contents (id, content_name, status, content_desc, content_type ) VALUES(?, ?, ?, ?, ?)`;

  let data = await new Promise((resolve) =>
    connection.query(
      sql,
      [id, content_name, status, contentDesc, contentType],
      function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        return rows.affectedRows > 0 ? resolve(true) : resolve(false);
      }
    )
  );

  if (!data) {
    return res.badData(data, "Data Content can't add to table", reply);
  }

  return res.ok(data, "Data Content added to table successfully", reply);
}

async function update(request, reply) {
  const id = request.params.id;
  const now = new Date();
  let queryBuilder = "";
  let arrValue = [];
  const contents = request.body;
  await Object.keys(contents).forEach(function (key) {
    queryBuilder = " " + queryBuilder + key + " = ? ,";
    arrValue.push(contents[key]);
  });
  let sql =
    "UPDATE contents SET " + queryBuilder + "updated_date = ? where id = ?";
  arrValue.push(now);
  arrValue.push(id);
  let data = await new Promise((resolve) =>
    connection.query(sql, arrValue, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Content can't update to table", reply);
  }

  return res.ok(data, "Data Content updated to table successfully", reply);
}

async function destroy(request, reply) {
  let id = request.params.id;
  let sql = `DELETE FROM contents WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Content can't delete from table", reply);
  }

  return res.ok(data, "Data Content deleted from table successfully", reply);
}
module.exports = {
  getAll,
  getById,
  create,
  update,
  destroy,
};
