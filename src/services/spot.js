let res = require("../response");
let connection = require("../plugins/mysqlConnection");
const util = require("../util");

async function getAll(request, reply) {
  const { content_type, limit, offset } = request.query;

  let sql = `SELECT
        s.id AS id,
        s.spot_name AS spot_name,
        s.province_id AS province_id,
        t1.name AS province_name,
        s.city_id AS city_id,
        t2.name AS city_name
    FROM
        spot AS s
        LEFT JOIN ( SELECT * FROM territories ) AS t1 ON t1.territory_code = s.province_id
        LEFT JOIN ( SELECT * FROM territories ) AS t2 ON t2.territory_code = s.city_id
    ORDER BY s.id DESC`;

  if (util.isParamsUndifined(request.query, 'limit', 'offset')) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }

  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.badData(data, "Data Spot can't be load", reply);
  }

  return res.ok(data, "Data Spot successfully to load", reply);
}

async function getById(request, reply) {
  let id = request.params.id;
  let sql = `SELECT
        s.id AS id,
        s.spot_name AS spot_name,
        s.province_id AS province_id,
        t1.name AS province_name,
        s.city_id AS city_id,
        t2.name AS city_name
    FROM
        spot AS s
        LEFT JOIN ( SELECT * FROM territories ) AS t1 ON t1.territory_code = s.province_id
        LEFT JOIN ( SELECT * FROM territories ) AS t2 ON t2.territory_code = s.city_id
    WHERE s.id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(data, "Spot ID can't be found", reply);
  }

  return res.ok(data, "Data Spot successfully to load", reply);
}

async function getByCity(request, reply) {
    let { id } = request.params;
    let sql = `SELECT
            s.id AS id,
            s.spot_name AS spot_name,
            s.province_id AS province_id,
            t1.name AS province_name,
            s.city_id AS city_id,
            t2.name AS city_name
        FROM
            spot AS s
            LEFT JOIN ( SELECT * FROM territories ) AS t1 ON t1.territory_code = s.province_id
            LEFT JOIN ( SELECT * FROM territories ) AS t2 ON t2.territory_code = s.city_id
        WHERE s.city_id = ?`;
  
    let data = await new Promise((resolve) =>
      connection.query(sql, [id], function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }
  
        return rows.length > 0 ? resolve(rows) : resolve(null);
      })
    );
  
    if (!data) {
      return res.notFound(data, "City ID can't be found", reply);
    }
  
    return res.ok(data, "City Spot successfully to load", reply);
  }

async function create(request, reply) {
  const {
    id,
    spot_name: spotName,
    province_id: provinceId,
    city_id: cityId,
  } = request.body;
  let sql = `INSERT INTO spot (id, spot_name, province_id, city_id) VALUES(?, ?, ?, ?)`;

  let data = await new Promise((resolve) =>
    connection.query(
      sql,
      [id, spotName, provinceId, cityId],
      function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        return rows.affectedRows > 0 ? resolve(true) : resolve(false);
      }
    )
  );

  if (!data) {
    return res.badData(data, "Data Spot can't add to table", reply);
  }

  return res.ok(data, "Data Spot added to table successfully", reply);
}

async function update(request, reply) {
  const id = request.params.id;
  const {
    spot_name: spotName,
    province_id: provinceId,
    city_id: cityId,
  } = request.body;
  let sql = `UPDATE spot SET spot_name = ?, province_id = ?, city_id = ? WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(
      sql,
      [spotName, provinceId, cityId, id],
      function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        return rows.affectedRows > 0 ? resolve(true) : resolve(false);
      }
    )
  );

  if (!data) {
    return res.badData(data, "Data Spot can't update to table", reply);
  }

  return res.ok(data, "Data Spot updated to table successfully", reply);
}

async function destroy(request, reply) {
  let id = request.params.id;
  let sql = `DELETE FROM spot WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Spot can't delete from table", reply);
  }

  return res.ok(data, "Data Spot deleted from table successfully", reply);
}
module.exports = {
  getAll,
  getById,
  getByCity,
  create,
  update,
  destroy,
};