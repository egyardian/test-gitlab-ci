let res = require("../response");
let connection = require("../plugins/mysqlConnection");
const util = require("../util");

async function getAll(request, reply) {
  const { territories, limit, offset } = request.query;
  let sql = `SELECT * FROM territories`;
  if (util.isParamsUndifined(request.query, 'limit', 'offset')) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }
  let data = await new Promise((resolve) =>
    connection.query(sql, [territories], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.badData(data, "Data Territories can't be load", reply);
  }

  return res.ok(data, "Data Territories successfully to load", reply);
}

async function getTerritories(request, reply) {
  const territories = request.params.id;
  let sql = `SELECT * FROM territories WHERE territory_code = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [territories], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(data, "Territories Master Level can't be found", reply);
  }

  return res.ok(data, "Data Territories successfully to load", reply);
}

async function getByMst(request, reply) {
  const { limit, offset } = request.query;
  const territoryCodeMst = request.params.id;
  let sql = `SELECT * FROM territories WHERE territory_code_mst = ?`;

  if (util.isParamsUndifined(request.query, 'limit', 'offset')) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }
  let data = await new Promise((resolve) =>
    connection.query(sql, [territoryCodeMst], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(data, "Territories Master Code can't be found", reply);
  }

  return res.ok(data, "Data Territories successfully to load", reply);
}

async function getByLevel(request, reply) {
  const level = request.params.id;
  const { limit, offset } = request.query;
  let sql = `SELECT * FROM territories WHERE level = ?`;

  if (util.isParamsUndifined(request.query, 'limit', 'offset')) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }
  let data = await new Promise((resolve) =>
    connection.query(sql, [level], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(data, "Territories Master Level can't be found", reply);
  }

  return res.ok(data, "Data Territories successfully to load", reply);
}

async function getCity(request, reply) {
  const { limit, offset } = request.query;
  let sql = `SELECT
	c.*,
	c2.NAME AS provinsi
FROM
	(
SELECT
	c.*,
	c.territory_code_mst AS c_territory_code_mst 
FROM
	territories AS c 
WHERE
	c.level = '2' 
ORDER BY
	c.territory_code 
	) AS c
	LEFT JOIN territories AS c2 ON c2.territory_code = c.c_territory_code_mst
WHERE
	1 = 1`;

  if (util.isParamsUndifined(request.query, 'limit', 'offset')) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }
  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(
      data,
      "Territories City Master Level can't be found",
      reply
    );
  }

  return res.ok(data, "Data Territories City successfully to load", reply);
}

async function getKec(request, reply) {
  const { limit, offset } = request.query;
  let sql = `SELECT
	c.*,
	c2.NAME AS kota,
	c3.NAME AS provinsi 
FROM
	(
SELECT
	c.*,
	c.territory_code_mst AS c_territory_code_mst 
FROM
	territories AS c 
WHERE
	c.level = '3' 
ORDER BY
	c.territory_code 
	) AS c
	LEFT JOIN territories AS c2 ON c2.territory_code = c.c_territory_code_mst
	LEFT JOIN territories AS c3 ON c3.territory_code = c2.territory_code_mst 
WHERE
	1 = 1`;

  if (util.isParamsUndifined(request.query, 'limit', 'offset')) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }
  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(
      data,
      "Territories Kecamatan Master Level can't be found",
      reply
    );
  }

  return res.ok(data, "Data Territories Kecamatan successfully to load", reply);
}

async function getKel(request, reply) {
  const { limit, offset } = request.query;
  let sql = `SELECT c.*, c2.NAME AS kecamatan, c3.NAME AS kota, c4.NAME AS provinsi FROM ( SELECT c.*, c.territory_code_mst AS c_territory_code_mst FROM territories AS c WHERE c.level = '4' ORDER BY c.territory_code ) AS c LEFT JOIN territories AS c2 ON c2.territory_code = c.c_territory_code_mst LEFT JOIN territories AS c3 ON c3.territory_code = c2.territory_code_mst LEFT JOIN territories AS c4 ON c4.territory_code = c3.territory_code_mst WHERE 1 = 1`;

  if (util.isParamsUndifined(request.query, 'limit', 'offset')) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }
  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(
      data,
      "Territories Kecamatan Master Level can't be found",
      reply
    );
  }

  return res.ok(data, "Data Territories Kecamatan successfully to load", reply);
}

module.exports = {
  getAll,
  getByMst,
  getByLevel,
  getTerritories,
  getCity,
  getKec,
  getKel,
};
