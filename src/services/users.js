let res = require("../response");
let connection = require("../plugins/mysqlConnection");

async function getAll(request, reply) {
  let sql = `SELECT * FROM users`;
  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      if (rows.length > 0) {
        return resolve(rows);
      } else {
        return resolve([]);
      }
    })
  );
  return res.ok(data, `success load data users`, reply);
}

async function getById(request, reply) {
  let id = request.params.id;
  let sql = `SELECT * FROM users WHERE role = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      if (rows.length > 0) {
        return resolve(rows);
      } else {
        return resolve("data not found");
      }
    })
  );

  return res.ok(data, " success load data users by Id", reply);
}

async function createuUserCourier(request, reply) {
  const {
    username,
    first_name: firstName,
    last_name: lastName,
    email,
    password,
    phone_number: phoneNumber,
    is_verified: isVerified,
  } = request.body;

  let sql = `INSERT INTO users (uid, username, password, email, phone_number,first_name,last_name,is_verified,role,created_date) values(UUID(),?,?,?,?,?,?,?,?,NOW())`;

  let data = await new Promise((resolve) =>
    connection.query(
      sql,
      [
        username,
        password,
        email,
        phoneNumber,
        firstName,
        lastName,
        isVerified,
        "courier",
      ],
      function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        return rows.affectedRows > 0 ? resolve(rows) : resolve(false);
      }
    )
  );

  if (!data) {
    return res.badData(data, "Data User Courier can't add to table", reply);
  }

  return res.ok(
    [data, dataimage],
    "Data User Courier added to table successfully",
    reply
  );
}

async function update(request, reply) {
  let id = request.params.id;
  let username = request.body.username;
  let password = request.body.password;
  let email = request.body.email;
  let phone_number = request.body.phone_number;
  let first_name = request.body.first_name;
  let last_name = request.body.last_name;
  let no_ktp = request.body.no_ktp;
  let valid_address = request.body.valid_address;
  let is_verified = request.body.is_verified;
  let role = request.body.role;
  let sql = `UPDATE users SET username =?, password =?, email=?, phone_number =?, first_name =?, last_name =?, no_ktp =?, valid_address =?, is_verified =?, role =?  WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(
      sql,
      [
        username,
        password,
        email,
        phone_number,
        first_name,
        last_name,
        no_ktp,
        valid_address,
        is_verified,
        role,
        id,
      ],
      function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        return rows.affectedRows > 0 ? resolve(true) : resolve(false);
      }
    )
  );

  let msg = data ? "Berhasil mengubah data!" : "Tidak berhasil mengubah data!";
  return res.ok(data, msg, reply);
}

async function destroy(request, reply) {
  let id = request.body.id;
  let sql = `DELETE FROM users WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  let msg = data
    ? "Berhasil Menghapus data!"
    : "Tidak berhasil Menghapus data!";
  return res.ok(data, msg, reply);
}
module.exports = {
  getAll,
  getById,
  createuUserCourier,
  update,
  destroy,
};
