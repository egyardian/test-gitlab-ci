let res = require("../response");
let connection = require("../plugins/mysqlConnection");

async function getAll(request, reply) {
  let sql = `SELECT * FROM courir_order`;
  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      if (rows.length > 0) {
        return resolve(rows);
      } else {
        return resolve([]);
      }
    })
  );
  return res.ok(data, `success load data courir_order`, reply);
}

async function getById(request, reply) {
  let id = request.params.id;
  let sql = `SELECT * FROM courir_order WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      if (rows.length > 0) {
        return resolve(rows);
      } else {
        return resolve("data not found");
      }
    })
  );

  return res.ok(data, " success load data courir_order by Id", reply);
}
//INPUT DATA ARRAY
async function create(request, reply) {
  let id = request.body.id;
  let order_id = request.body.order_id;
  let count_id = request.body.count_id;
  let rating = request.body.rating;

  let sql = `insert into courir_order (id,order_id, count_id, rating) value (?,?,?,?)`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id, order_id, count_id, rating], function (
      error,
      rows
    ) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  let msg = [
    data ? "Berhasil menambahkan data!" : "Tidak berhasil menambahkan data!",
  ];
  return res.ok(data, msg, reply);
}

async function update(request, reply) {
  let id = request.params.id;
  let order_id = request.body.order_id;
  let count_id = request.body.count_id;
  let rating = request.body.rating;
  let sql = `UPDATE courir_order SET order_id =?, count_id =?, rating =? WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [order_id, count_id, rating, id], function (
      error,
      rows
    ) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  let msg = data ? "Berhasil mengubah data!" : "Tidak berhasil mengubah data!";
  return res.ok(data, msg, reply);
}

async function destroy(request, reply) {
  let id = request.body.id;
  let sql = `DELETE FROM courir_order WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  let msg = data
    ? "Berhasil Menghapus data!"
    : "Tidak berhasil Menghapus data!";
  return res.ok(data, msg, reply);
}
module.exports = {
  getAll,
  getById,
  create,
  update,
  destroy,
};
