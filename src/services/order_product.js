let res = require("../response");
let connection = require("../plugins/mysqlConnection");

async function getAll(request, reply) {
  let sql = `SELECT * FROM orders_products`;
  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      if (rows.length > 0) {
        return resolve(rows);
      } else {
        return resolve([]);
      }
    })
  );
  return res.ok(data, `success load data orders_products`, reply);
}

async function getById(request, reply) {
  let id = request.params.id;
  let sql = `SELECT * FROM orders_products WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      if (rows.length > 0) {
        return resolve(rows);
      } else {
        return resolve("data not found");
      }
    })
  );

  return res.ok(data, " success load data orders_products by Id", reply);
}
//INPUT DATA ARRAY
async function create(request, reply) {
  let id = request.body.id;
  let order_id = request.body.order_id;
  let product_id = request.body.product_id;
  let quantity = request.body.quantity;
  let price = request.body.price;
  let rating = request.body.rating;
  let comments = request.body.comments;

  let sql = `insert into orders_products (id,order_id, product_id, quantity, price, rating, comments) value (?,?,?,?,?,?,?)`;

  let data = await new Promise((resolve) =>
    connection.query(
      sql,
      [id, order_id, product_id, quantity, price, rating, comments],
      function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        return rows.affectedRows > 0 ? resolve(true) : resolve(false);
      }
    )
  );

  let msg = [
    data ? "Berhasil menambahkan data!" : "Tidak berhasil menambahkan data!",
  ];
  return res.ok(data, msg, reply);
}

async function update(request, reply) {
  let id = request.params.id;
  let order_id = request.body.order_id;
  let product_id = request.body.product_id;
  let quantity = request.body.quantity;
  let price = request.body.price;
  let rating = request.body.rating;
  let comments = request.body.comments;
  let sql = `UPDATE orders_products SET order_id =?, product_id =?, quantity =?, price =?, rating =?, comments =?  WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(
      sql,
      [order_id, product_id, quantity, price, rating, comments, id],
      function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        return rows.affectedRows > 0 ? resolve(true) : resolve(false);
      }
    )
  );

  let msg = data ? "Berhasil mengubah data!" : "Tidak berhasil mengubah data!";
  return res.ok(data, msg, reply);
}

async function destroy(request, reply) {
  let id = request.body.id;
  let sql = `DELETE FROM orders_products WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  let msg = data
    ? "Berhasil Menghapus data!"
    : "Tidak berhasil Menghapus data!";
  return res.ok(data, msg, reply);
}
module.exports = {
  getAll,
  getById,
  create,
  update,
  destroy,
};
