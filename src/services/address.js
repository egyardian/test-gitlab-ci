let res = require("../response");
let connection = require("../plugins/mysqlConnection");
const util = require("../util");

async function getAll(request, reply) {
  const { limit, offset } = request.query;

  let sql = `SELECT * FROM addresses ORDER BY id DESC`;

  if (util.isParamsUndifined(request.query, 'limit', 'offset')) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }

  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.badData(data, "Data Address can't be load", reply);
  }

  return res.ok(data, "Data Address successfully to load", reply);
}

async function getById(request, reply) {
  let id = request.params.id;
  let sql = `SELECT * FROM addresses WHERE source_id=?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(data, "Address ID can't be found", reply);
  }

  return res.ok(data, "Data Address successfully to load", reply);
}

async function create(request, reply) {
  const {
    source_id,
    province: province,
    city: city,
    kecamatan: kecamatan,
    kelurahan: kelurahan,
    postal_code: postalCode,
    detail_address: detailAddress,
    latitude,
    longitude,
    spot_id: spotId,
  } = request.body;

  let sql = `INSERT INTO addresses ( source_id, province, city, kecamatan, kelurahan, postal_code, detail_address, latitude, longitude, spot_id ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )`;

  let data = await new Promise((resolve) =>
    connection.query(
      sql,
      [
        source_id,
        province,
        city,
        kecamatan,
        kelurahan,
        postalCode,
        detailAddress,
        latitude,
        longitude,
        spotId,
      ],
      function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        return rows.affectedRows > 0 ? resolve(rows) : resolve(false);
      }
    )
  );

  if (!data) {
    return res.badData(data, "Data Address can't add to table", reply);
  }

  return res.ok(data, "Data Address added to table successfully", reply);
}

async function update(request, reply) {
  const id = request.body.id;
  const sourceId = request.body.source_id;
  const now = new Date();
  let queryBuilder = "";
  let arrValue = [];
  const adsress = request.body;
  await Object.keys(adsress).forEach(function (key) {
    queryBuilder = " " + queryBuilder + key + " = ? ,";
    arrValue.push(adsress[key]);
  });
  let sql =
    "UPDATE addresses SET " +
    queryBuilder +
    "updated_date = ? where id = ? and source_id = ?";
  arrValue.push(now);
  arrValue.push(id);
  arrValue.push(sourceId);
  let data = await new Promise((resolve) =>
    connection.query(sql, arrValue, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Address can't update to table", reply);
  }

  return res.ok(data, "Data Address updated to table successfully", reply);
}

async function destroy(request, reply) {
  let id = request.params.id;
  let sql = `DELETE FROM addresses WHERE source_id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Address can't delete from table", reply);
  }

  return res.ok(data, "Data Address deleted from table successfully", reply);
}
module.exports = {
  getAll,
  getById,
  create,
  update,
  destroy,
};
