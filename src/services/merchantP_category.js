let res = require("../response");
let connection = require("../plugins/mysqlConnection");

const util = require("../util");

async function getAll(request, reply) {
  const { limit, offset } = request.params;

  let sql = `SELECT * FROM merchant_product_category`;

  if (util.isParamsUndifined(request.query, "limit", "offset")) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }
  try {
    let data = await new Promise((resolve) =>
      connection.query(sql, function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        if (rows.length > 0) {
          return resolve(rows);
        } else {
          return resolve(null);
        }
      })
    );
    if (!data) {
      return res.badData(
        data,
        "Data Merchant Product Category can't be load",
        reply
      );
    }

    return res.ok(
      data,
      "Data Merchant Product Category successfully to load",
      reply
    );
  } catch (error) {
    console.log("error");
    return Promise.reject(error);
  }
}

async function getById(request, reply) {
  let id = request.params.id;
  let sql = `SELECT * FROM merchant_product_category WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(
      data,
      "Data Merchant Product Category can't be found",
      reply
    );
  }

  return res.ok(
    data,
    "Data Merchant Product Category successfully to load",
    reply
  );
}

async function create(request, reply) {
  const {
    category_name: categoryName,
    merchant_id: merchantId,
    product_id: productId,
  } = request.body;

  let sql = `INSERT INTO merchant_product_category (category_name, merchant_id,product_id) values( ?,?,?)`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [categoryName, merchantId, productId], function (
      error,
      rows
    ) {
      if (error) {
        console.log(error);
        return res.badRequest(false, `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(
      data,
      "Data Merchant Product Category can't add to table",
      reply
    );
  }

  return res.ok(
    data,
    "Data Merchant Product Category added to table successfully",
    reply
  );
}

async function update(request, reply) {
  const id = request.params.id;
  const now = new Date();
  let queryBuilder = "";
  let arrValue = [];
  const merchantP = request.body;
  await Object.keys(merchantP).forEach(function (key) {
    queryBuilder = " " + queryBuilder + key + " = ? ,";
    arrValue.push(merchantP[key]);
  });
  let sql =
    "UPDATE merchant_product_category SET " +
    queryBuilder +
    "updated_date = ? where id = ?";
  arrValue.push(now);
  arrValue.push(id);
  let data = await new Promise((resolve) =>
    connection.query(sql, arrValue, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(
      data,
      "Data Merchant Product Category can't update to table",
      reply
    );
  }

  return res.ok(
    data,
    "Data Merchant Product Category updated to table successfully",
    reply
  );
}

async function destroy(request, reply) {
  let id = request.params.id;
  let sql = `DELETE FROM merchant_product_category WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(
      data,
      "Data Merchant Product Category can't delete from table",
      reply
    );
  }

  return res.ok(
    data,
    "Data Merchant Product Category deleted from table successfully",
    reply
  );
}
module.exports = {
  getAll,
  getById,
  create,
  update,
  destroy,
};

// INPUT DATA ARRAY
// async function create(request, reply) {
//     const categories = request.body.category;
//     let queryParams = [];
//     for (var i = 0; i < categories.length; i++) {
//         const categori = categories[i];
//         let id = categori.id;
//         let category_name = categori.category_name;
//         let merchant_id = categori.merchant_id;

//         let param = [
//             id,
//             category_name,
//             merchant_id
//         ];

//         queryParams.push(param);
//     }
//     let sql = `INSERT INTO Categories (id, category_name, merchant_id) values ?`;

//     let data = await new Promise((resolve) =>
//         connection.query(sql,Koléga x MarkPlus, Satrio, Segitiga Emas Business Park Jalan Prof. Dr. Satrio Kuningan, Setiabudi RT.4/RW.4 Jakarta Selatan DKI Jakarta 12940
//             [queryParams],
//             function (error, rows) {
//                 if (error) {
//                     console.log(error);
//                     return res.badRequest('', `${error}`, reply)
//                 }

//                 return rows.affectedRows > 0 ? resolve(true) : resolve(false);
//             })
//     );

//     let msg = data ? "Berhasil menambahkan data!" : "Tidak berhasil menambahkan data!";
//     return res.ok(data, msg, reply);
// }
