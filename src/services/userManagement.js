let res = require("../response");
let connection = require("../plugins/mysqlConnection");
let moment = require("moment");
const util = require("../util");

async function getAll(request, reply) {
  const { limit, offset } = request.query;
  let sql = `SELECT
            um.user_id AS user_id,
            CONCAT(u.first_name, ' ', u.last_name) AS name,
            t1.name AS wilayah,
            t2.name AS area
            FROM 
            user_management as um 
              INNER JOIN users as u on u.uid = um.user_id
              LEFT JOIN ( SELECT * FROM territories ) AS t1 ON t1.territory_code = um.province
              LEFT JOIN ( SELECT * FROM territories ) AS t2 ON t2.territory_code = um.city`;
  if (util.isParamsUndifined(request.query, "limit", "offset")) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }

  try {
    let data = await new Promise((resolve) =>
      connection.query(sql, function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        if (rows.length > 0) {
          return resolve(rows);
        } else {
          return resolve(null);
        }
      })
    );

    if (!data) {
      return res.badData(data, "Data User Management can't be load", reply);
    }

    return res.ok(data, "Data User Management successfully to load", reply);
  } catch (error) {
    console.log("error");
    return Promise.reject(error);
  }
}

async function getById(request, reply) {
  const id = request.params.id;
  const { limit, offset } = request.query;

  let sql = `SELECT
              um.user_id AS user_id,
              CONCAT(u.first_name, ' ', u.last_name) AS name,
              t1.name AS wilayah,
              t2.name AS area,
              um.province,
              um.city,
              menu
            FROM 
            user_management as um 
              INNER JOIN users as u on u.uid = um.user_id
              LEFT JOIN ( SELECT * FROM territories ) AS t1 ON t1.territory_code = um.province
              LEFT JOIN ( SELECT * FROM territories ) AS t2 ON t2.territory_code = um.city WHERE um.user_id = ?`;
  if (util.isParamsUndifined(request.query, "limit", "offset")) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }

  try {
    let data = await new Promise((resolve) =>
      connection.query(sql, id, function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        if (rows.length > 0) {
          return resolve(rows);
        } else {
          return resolve(null);
        }
      })
    );

    if (!data) {
      return res.badData(data, "Data User Management can't be load", reply);
    }

    return res.ok(data, "Data User Management successfully to load", reply);
  } catch (error) {
    console.log("error");
    return Promise.reject(error);
  }
}

async function create(request, reply) {
  const { user_id: userId, role, province, city, menu } = request.body;

  let sql = `INSERT INTO user_management ( user_id, role, province, city, menu) values(?,?,?,?,?)`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [userId, role, province, city, menu], function (
      error,
      rows
    ) {
      if (error) {
        console.log(error);
        return res.badRequest(false, `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data User Management can't add to table", reply);
  }

  return res.ok(
    data,
    "Data User Management added to table successfully",
    reply
  );
}

async function update(request, reply) {
  const id = request.params.id;
  const now = new Date();
  let queryBuilder = "";
  let arrValue = [];
  const userManagement = request.body;
  await Object.keys(userManagement).forEach(function (key) {
    queryBuilder = " " + queryBuilder + key + " = ? ,";
    arrValue.push(userManagement[key]);
  });
  let sql =
    "UPDATE user_management SET " +
    queryBuilder +
    "updated_date = ? where user_id = ?";
  arrValue.push(now);
  arrValue.push(id);
  let data = await new Promise((resolve) =>
    connection.query(sql, arrValue, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(
      data,
      "Data User Management can't update to table",
      reply
    );
  }

  return res.ok(
    data,
    "Data User Management updated to table successfully",
    reply
  );
}

async function destroy(request, reply) {
  let id = request.params.id;
  let sql = `DELETE FROM user_management WHERE user_id = ?`;
  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(
      data,
      "Data User Mangement can't delete from table",
      reply
    );
  }

  return res.ok(
    data,
    "Data User Mangement deleted from table successfully",
    reply
  );
}

module.exports = {
  getAll,
  create,
  update,
  getById,
  destroy,
};
