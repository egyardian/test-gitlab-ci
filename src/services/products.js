let res = require("../response");
let connection = require("../plugins/mysqlConnection");
const util = require("../util");

async function getAll(request, reply) {
  const { productName, categoryId, merchantId, limit, offset } = request.query;

  let sql = `SELECT 
        products.id,
        product_name,
        products.hpp,
        products.price,
        products.quantity,
        products.rating,
        categories.category_name,
        merchants.merchant_name,
        products.image_url 
    FROM 
        products
        INNER JOIN categories ON categories.id = products.category_id 
        INNER JOIN merchants ON merchants.id = products.merchant_id WHERE`;

  await Object.keys(request.query).forEach((key) => {
    if (!util.isParamsNull(request.query, key)) {
      let coloum = util.camelToSnakeCase(key);
      if (key != "limit" && key != "offset") {
        let tables = "products";
        if (key != "product_name") {
          sql += " " + tables + `.${coloum} = ${request.query[key]} AND`;
        } else {
          sql += " " + tables + `.${coloum} LIKE "%${request.query[key]}%" AND`;
        }
      }
    }
  });

  // Check if last char in query is 'WHERE' or 'AND' then remove it.
  sql =
    sql.charAt(sql.length - 1) == "E"
      ? sql.substring(0, sql.length - 6)
      : sql.substring(0, sql.length - 4);
  sql += ` ORDER BY products.id DESC`;

  if (
    util.isParamsUndifined(request.query, "limit", "offset") &&
    !util.isParamsNull(request.query, "limit", "offset")
  ) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }

  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.badData(data, "Data Product can't be load", reply);
  }

  return res.ok(data, "Data Product successfully to load", reply);
}

async function getById(request, reply) {
  let id = request.params.id;
  let sql = `SELECT *, products.description, products.image_url FROM products INNER JOIN categories ON products.category_id = categories.id INNER JOIN merchants ON products.merchant_id = merchants.id Where products.id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }
      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(data, "Data products can't be found", reply);
  }

  return res.ok(data, "Data products successfully to load", reply);
}

async function getByMerchantId(request, reply) {
  let id = request.params.id;
  let sql = `SELECT * FROM products INNER JOIN categories ON products.category_id = categories.id INNER JOIN merchants ON products.merchant_id = merchants.id Where products.merchant_id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }
      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(data, "Data products can't be found", reply);
  }

  return res.ok(data, "Data products successfully to load", reply);
}
async function create(request, reply) {
  const {
    merchant_id: merchantId,
    category_id: categoryId,
    merchant_category_id: merchantCategoryId,
    product_name: productName,
    description,
    price,
    rating,
    quantity,
    image_url: imageUrl,
    hpp,
  } = request.body;

  let sql = `INSERT INTO products ( merchant_id, category_id,product_name,merchant_category_id,description,price,quantity,rating, image_url,hpp) values(?, ?,?,?,?,?,?,?,?,?)`;

  let data = await new Promise((resolve) =>
    connection.query(
      sql,
      [
        merchantId,
        categoryId,
        productName,
        merchantCategoryId,
        description,
        price,
        quantity,
        rating,
        imageUrl,
        hpp,
      ],
      function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest(false, `${error}`, reply);
        }

        return rows.affectedRows > 0 ? resolve(rows) : resolve(false);
      }
    )
  );

  if (!data) {
    return res.badData(data, "Data products can't add to table", reply);
  }

  return res.ok(data, "Data products added to table successfully", reply);
}

async function update(request, reply) {
  const id = request.params.id;
  const now = new Date();
  let queryBuilder = "";
  let arrValue = [];
  const products = request.body;
  await Object.keys(products).forEach(function (key) {
    queryBuilder = " " + queryBuilder + key + " = ? ,";
    arrValue.push(products[key]);
  });
  let sql =
    "UPDATE products SET " + queryBuilder + "updated_date = ? where id = ?";
  arrValue.push(now);
  arrValue.push(id);
  let data = await new Promise((resolve) =>
    connection.query(sql, arrValue, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );
  if (!data) {
    return res.badData(data, "Data products can't update to table", reply);
  }

  return res.ok(data, "Data products updated to table successfully", reply);
}

async function destroy(request, reply) {
  let id = request.params.id;
  let sql = `DELETE products FROM products WHERE id = ?`;
  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data products can't delete from table", reply);
  }

  return res.ok(data, "Data products deleted from table successfully", reply);
}
module.exports = {
  getAll,
  getById,
  create,
  update,
  destroy,
  getByMerchantId,
};
