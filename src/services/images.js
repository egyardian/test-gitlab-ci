const response = require("../response");
let connection = require("../plugins/mysqlConnection");
let res = require("../response");
const util = require("../util");

async function getAll(request, reply) {
  const { limit, offset } = request.params;
  let sql = `SELECT * FROM images`;
  if (util.isParamsUndifined(request.query, 'limit', 'offset')) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }
  try {
    let data = await new Promise((resolve) =>
      connection.query(sql, [limit, offset], function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        if (rows.length > 0) {
          return resolve(rows);
        } else {
          return resolve(null);
        }
      })
    );

    if (!data) {
      return response.badData(data, "Data Image can't be load", reply);
    }

    return response.ok(data, "Data Image successfully to load", reply);
  } catch (error) {
    console.log("error");
    return Promise.reject(error);
  }
}

async function getById(request, reply) {
  let id = request.params.id;
  let sql = `SELECT * FROM images WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return response.notFound(data, "Data Image can't be found", reply);
  }

  return response.ok(data, "Data Image successfully to load", reply);
}

async function create(request, reply) {
  const {
    image_url: imageUrl,
    image_base64: imageBase64,
    description,
  } = request.body;
  let sql = `INSERT INTO images (image_url,image_base64, description) values( ?, ?, ?)`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [imageUrl, imageBase64, description], function (
      error,
      rows
    ) {
      if (error) {
        console.log(error);
        return res.badRequest(false, `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Image can't add to table", reply);
  }

  return res.ok(data, "Data Image added to table successfully", reply);
}

async function update(request, reply) {
  let id = request.params.id;
  const {
    image_url: imageUrl,
    image_base64: imageBase64,
    description,
  } = request.body;
  let sql = `UPDATE images SET image_url = ?, image_base64=?, description = ? WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [imageUrl, imageBase64, description, id], function (
      error,
      rows
    ) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Image can't update to table", reply);
  }

  return res.ok(data, "Data Image updated to table successfully", reply);
}

async function destroy(request, reply) {
  let id = request.body.id;
  let sql = `DELETE FROM images WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Image can't delete from table", reply);
  }

  return res.ok(data, "Data Image deleted from table successfully", reply);
}
module.exports = {
  getAll,
  getById,
  create,
  update,
  destroy,
};
