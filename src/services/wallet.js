let res = require("../response");
let connection = require("../plugins/mysqlConnection");
let moment = require("moment");
const util = require("../util");

async function getAll(request, reply) {
  const id = request.params.id;
  const { limit, offset } = request.query;

  let sql = `SELECT *,wallet.id FROM wallet inner join users on users.uid = wallet.user_id where wallet.user_id = ?`;
  if (util.isParamsUndifined(request.query, "limit", "offset")) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }

  try {
    let data = await new Promise((resolve) =>
      connection.query(sql, id, function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        if (rows.length > 0) {
          let array = [];
          rows.forEach((element) => {
            array.push({
              id: element.id,
              user_id: element.user_id,
              first_name: element.first_name,
              last_name: element.last_name,
              value: element.value,
            });
          });

          return resolve(array);
        } else {
          return resolve(null);
        }
      })
    );

    if (!data) {
      return res.badData(data, "Data Wallet can't be load", reply);
    }

    return res.ok(data, "Data Wallet successfully to load", reply);
  } catch (error) {
    console.log("error");
    return Promise.reject(error);
  }
}

async function create(request, reply) {
  const { user_id: userId, value } = request.body;

  const now = new Date();

  let sql = `INSERT INTO wallet ( user_id, value, created_date) values(?,?,?)`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [userId, value, now], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest(false, `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Wallet can't add to table", reply);
  }

  return res.ok(data, "Data Wallet added to table successfully", reply);
}

async function update(request, reply) {
  const id = request.params.id;
  const now = new Date();
  let queryBuilder = "";
  let arrValue = [];
  const wallet = request.body;
  await Object.keys(wallet).forEach(function (key) {
    queryBuilder = " " + queryBuilder + key + " = ? ,";
    arrValue.push(wallet[key]);
  });
  let sql =
    "UPDATE wallet SET " + queryBuilder + "updated_date = ? where id = ?";
  arrValue.push(now);
  arrValue.push(id);
  let data = await new Promise((resolve) =>
    connection.query(sql, arrValue, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Wallet can't update to table", reply);
  }

  return res.ok(data, "Data Wallet updated to table successfully", reply);
}

module.exports = {
  getAll,
  create,
  update,
};
