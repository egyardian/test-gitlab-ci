let res = require("../response");
let connection = require("../plugins/mysqlConnection");
const util = require("../util");

async function getAll(request, reply) {
  const { limit, offset } = request.query;

  let sql = `SELECT
        m.id AS id,
        m.merchant_name AS merchant_name,
        m.status AS status,
        m.merchant_pic AS merchant_pic,
        m.pic_contact AS pic_contact,
        a.detail_address AS address,
        t1.name AS province,
        t2.name AS city,
        t3.name AS kecamatan,
        t4.name AS kelurahan,
        s.spot_name AS spot
      FROM
        merchants AS m
        INNER JOIN ( SELECT * FROM addresses ) AS a ON a.source_id = m.id
        LEFT JOIN ( SELECT * FROM territories ) AS t1 ON t1.territory_code = a.province
        LEFT JOIN ( SELECT * FROM territories ) AS t2 ON t2.territory_code = a.city
        LEFT JOIN ( SELECT * FROM territories ) AS t3 ON t3.territory_code = a.kecamatan
        LEFT JOIN ( SELECT * FROM territories ) AS t4 ON t4.territory_code = a.kelurahan
        LEFT JOIN ( SELECT * FROM spot ) AS s ON s.id = a.spot_id WHERE`;

  await Object.keys(request.query).forEach((key) => {
    if (!util.isParamsNull(request.query, key)) {
      if (key != "limit" && key != "offset") {
        let tables =
          key == "merchant_name" ||
          key == "merchant_pic" ||
          key == "pic_contact"
            ? "m"
            : "a";
        if (key != "merchant_name" && key != "merchant_pic") {
          sql += " " + tables + `.${key} = ${request.query[key]} AND`;
        } else {
          sql += " " + tables + `.${key} LIKE "%${request.query[key]}%" AND`;
        }
      }
    }
  });

  // Check if last char in query is 'WHERE' or 'AND' then remove it.
  sql =
    sql.charAt(sql.length - 1) == "E"
      ? sql.substring(0, sql.length - 6)
      : sql.substring(0, sql.length - 4);
  sql += ` ORDER BY m.id DESC`;

  if (
    util.isParamsUndifined(request.query, "limit", "offset") &&
    !util.isParamsNull(request.query, "limit", "offset")
  ) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }
  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.badData(data, "Data Merchant can't be load", reply);
  }

  return res.ok(data, "Data Merchant successfully to load", reply);
}

async function getAllMerchantWithSchedule(request, reply) {
  const { limit, offset } = request.query;

  let sql = `SELECT
    merc.*,
    schedule.merchant_id as schedule_merchant_id,
    schedule.day,
    schedule.start_time,
    schedule.end_time
  FROM  (
     SELECT merchants.* FROM merchants
     WHERE
           merchants.status = 1 AND
           merchants.is_active = 1
     ORDER  BY merchants.id ASC
     ) merc
  JOIN schedule ON schedule.merchant_id = merc.id AND schedule.status = 1;`;

  if (util.isParamsUndifined(request.query, "limit", "offset")) {
    sql = util.insertString(
      sql,
      `LIMIT ${limit} OFFSET ${offset}`,
      sql.indexOf("ASC") + 4
    );
  }

  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      if (rows.length > 0) {
        let array = [];
        rows.forEach((element) => {
          array.push({
            id: element.id,
            merchant_name: element.merchant_name,
            merchant_pic: element.merchant_pic,
            pic_contact: element.pic_contact,
            status: element.status,
            is_active: element.is_active,
            address_id: element.address_id,
            user_id: element.user_id,
            schedules: {
              schedule_id: element.schedule_id,
              day: element.day,
              start_time: element.start_time,
              end_time: element.end_time,
            },
          });
        });

        return resolve(util.mergeObject(array, 1));
      }

      return resolve(null);
    })
  );

  if (!data) {
    return res.badData(data, "Data Merchant can't be load", reply);
  }

  return res.ok(data, "Data Merchant successfully to load", reply);
}

async function getById(request, reply) {
  let id = request.params.id;
  let sql = `SELECT * FROM merchants WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(data, "Merchant ID can't be found", reply);
  }

  return res.ok(data, "Data Merchant successfully to load", reply);
}

async function create(request, reply) {
  const {
    merchant_name: merchantName,
    merchant_pic: merchantPIC,
    pic_contact: picContact,
    status,
    is_active: isActive,
    user_id: userId,
    address_id: addressId,
  } = request.body;

  let sql = `INSERT INTO merchants ( merchant_name, merchant_pic, pic_contact, status, is_active, address_id, user_id ) VALUES ( ?, ?, ?, ?, ?, ?, ?)`;
  try {
    let data = await new Promise((resolve) =>
      connection.query(
        sql,
        [
          merchantName,
          merchantPIC,
          picContact,
          status,
          isActive,
          addressId,
          userId,
        ],
        function (error, rows) {
          if (error) {
            console.log(error);
            return res.badRequest(false, `${error}`, reply);
          }

          return rows.affectedRows > 0 ? resolve(rows) : resolve(false);
        }
      )
    );

    if (!data) {
      return res.badData(data, "Data Merchant can't add to table", reply);
    }

    return res.ok(data, "Data Merchant added to table successfully", reply);
  } catch (error) {
    console.log("error");
    return Promise.reject(error);
  }
}

async function update(request, reply) {
  let id = request.params.id;
  const now = new Date();
  let queryBuilder = "";
  let arrValue = [];
  const merchants = request.body;
  await Object.keys(merchants).forEach(function (key) {
    queryBuilder = " " + queryBuilder + key + " = ? ,";
    arrValue.push(merchants[key]);
  });
  let sql =
    "UPDATE merchants SET " + queryBuilder + "updated_date = ? where id = ?";
  arrValue.push(now);
  arrValue.push(id);
  let data = await new Promise((resolve) =>
    connection.query(sql, arrValue, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Merchant can't update to table", reply);
  }

  return res.ok(data, "Data Merchant updated to table successfully", reply);
}

async function destroy(request, reply) {
  let id = request.params.id;
  let idA = request.params.id;
  let idS = request.params.id;
  let sql = `delete m,a,s FROM merchants as m inner join addresses as a on a.source_id = m.id inner join schedule as s on s.merchant_id = m.id where m.id = ? and a.source_id = ? and s.merchant_id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id, idA, idS], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Merchants can't delete from table", reply);
  }

  return res.ok(data, "Data Merchants deleted from table successfully", reply);
}

async function deleted(request, reply) {
  let id = request.params.id;
  let sql = `DELETE * FROM merchants where id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Merchant can't delete from table", reply);
  }

  return res.ok(data, "Data Merchant deleted from table successfully", reply);
}
module.exports = {
  getAll,
  getAllMerchantWithSchedule,
  getById,
  create,
  update,
  destroy,
};
