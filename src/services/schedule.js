let res = require("../response");
let connection = require("../plugins/mysqlConnection");

const util = require("../util");

async function getAll(request, reply) {
  const { limit, offset } = request.query;
  let sql = `SELECT * FROM schedule ORDER BY id DESC`;

  if (util.isParamsUndifined(request.query, 'limit', 'offset')) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }
  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.badData(data, "Data schedule can't be load", reply);
  }

  return res.ok(data, "Data schedule successfully to load", reply);
}

async function getByIdMerchant(request, reply) {
  let id = request.params.id;
  let sql = `SELECT * FROM schedule WHERE merchant_id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(data, "Schedule ID can't be found", reply);
  }

  return res.ok(data, "Schedule Category successfully to load", reply);
}

async function create(request, reply) {
  const schedules = request.body.schedule;
  let queryParams = [];
  for (var i = 0; i < schedules.length; i++) {
    const schedule = schedules[i];
    let merchantId = schedule.merchant_id;
    let day = schedule.day;
    let startTime = schedule.start_time;
    let endTime = schedule.end_time;
    let status = schedule.status;
    let param = [merchantId, day, startTime, endTime, status];

    queryParams.push(param);
  }
  let sql = `INSERT INTO schedule (merchant_id, day,start_time,end_time,status) values ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [queryParams], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(rows) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data schedule can't add to table", reply);
  }

  return res.ok(data, "Data schedule added to table successfully", reply);
}

async function update(request, reply) {
  const schedules = request.body.schedule;
  let queryParams = [];
  for (var i = 0; i < schedules.length; i++) {
    const schedule = schedules[i];
    let day = schedule.day;
    let startTime = schedule.start_time;
    let endTime = schedule.end_time;
    let status = schedule.status;
    let merchantId = schedule.merchant_id;
    let id = schedule.id;
    let param = [id, merchantId, day, startTime, endTime, status];
    queryParams.push(param);
  }

  console.log(queryParams);

  let sql = `INSERT into schedule (id, merchant_id, day, start_time, end_time, status)
            VALUES ?
            ON DUPLICATE KEY UPDATE
                day = VALUES(day),
                start_time = VALUES(start_time),
                end_time = VALUES(end_time),
                status = VALUES(status)`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [queryParams], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Schedule can't update to table", reply);
  }

  return res.ok(data, "Data Schedule updated to table successfully", reply);
}

async function deleted(request, reply) {
  let id = request.params.id;
  let sql = `DELETE * FROM schedule where merchant_id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Schedule can't delete from table", reply);
  }

  return res.ok(data, "Data Schedule deleted from table successfully", reply);
}

module.exports = {
  getAll,
  getByIdMerchant,
  create,
  update,
  deleted,
};
