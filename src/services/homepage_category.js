let res = require("../response");
let connection = require("../plugins/mysqlConnection");
const util = require("../util");

async function getAll(request, reply) {
  const { limit, offset } = request.query;

  let sql = `SELECT h.*,p.product_name,m.merchant_name FROM homepage_category as h inner join products as p on h.product_id=p.id inner join merchants as m on h.merchant_id = m.id`;

  if (util.isParamsUndifined(request.query, 'limit', 'offset')) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }

  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.badData(data, "Data Home Page Category can't be load", reply);
  }

  return res.ok(data, "Data Home Page Category successfully to load", reply);
}

async function getById(request, reply) {
  let id = request.params.id;
  let sql = `SELECT h.*,p.product_name,m.merchant_name FROM homepage_category as h inner join products as p on h.product_id=p.id inner join merchants as m on h.merchant_id = m.id WHERE h.id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0 ? resolve(rows) : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(data, "Home Page Category ID can't be found", reply);
  }

  return res.ok(data, "Data Home Page Category successfully to load", reply);
}

async function create(request, reply) {
  const {
    merchant_id: merchantId,
    product_id: productId,
    page_category: pageCategory,
    valid_start: validStart,
    valid_end: validEnd,
  } = request.body;

  let sql = `INSERT INTO homepage_category ( merchant_id, product_id, page_category, valid_start, valid_end ) VALUES(?, ?, ?, ?, ?)`;

  let data = await new Promise((resolve) =>
    connection.query(
      sql,
      [merchantId, productId, pageCategory, validStart, validEnd],
      function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        return rows.affectedRows > 0 ? resolve(true) : resolve(false);
      }
    )
  );

  if (!data) {
    return res.badData(
      data,
      "Data Home Page Category can't add to table",
      reply
    );
  }

  return res.ok(
    data,
    "Data Home Page Category added to table successfully",
    reply
  );
}

async function update(request, reply) {
  const id = request.params.id;
  const now = new Date();
  let queryBuilder = "";
  let arrValue = [];
  const homepage = request.body;
  await Object.keys(homepage).forEach(function (key) {
    queryBuilder = " " + queryBuilder + key + " = ? ,";
    arrValue.push(homepage[key]);
  });
  let sql =
    "UPDATE homepage_category SET " +
    queryBuilder +
    "updated_date = ? where id = ?";
  arrValue.push(now);
  arrValue.push(id);
  let data = await new Promise((resolve) =>
    connection.query(sql, arrValue, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(
      data,
      "Data Home Page Category can't update to table",
      reply
    );
  }

  return res.ok(
    data,
    "Data Home Page Category updated to table successfully",
    reply
  );
}

async function destroy(request, reply) {
  let id = request.body.id;
  let sql = `DELETE FROM homepage_category WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(
      data,
      "Data Home Page Category can't delete from table",
      reply
    );
  }

  return res.ok(
    data,
    "Data Home Page Category deleted from table successfully",
    reply
  );
}
module.exports = {
  getAll,
  getById,
  create,
  update,
  destroy,
};
