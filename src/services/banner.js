let res = require("../response");
let connection = require("../plugins/mysqlConnection");
let moment = require("moment");
const util = require("../util");

async function getAll(request, reply) {
  const { limit, offset } = request.query;

  let sql = `SELECT * FROM banner ORDER BY priority DESC`;
  if (util.isParamsUndifined(request.query, "limit", "offset")) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }

  try {
    let data = await new Promise((resolve) =>
      connection.query(sql, function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        if (rows.length > 0) {
          let array = [];
          rows.forEach((element) => {
            array.push({
              id: element.id,
              title: element.title,
              description: element.description,
              mobile_type: element.mobile_type,
              action_url: element.action_url,
              merchant_id: element.merchant_id,
              product_id: element.product_id,
              start_date: moment(element.start_date)
                .format("YYYY-MM-DD")
                .toString(),
              end_date: moment(element.end_date)
                .format("YYYY-MM-DD")
                .toString(),
              image_url: element.image_url,
              priority: element.priority,
              status: element.status,
            });
          });

          return resolve(array);
        } else {
          return resolve(null);
        }
      })
    );

    if (!data) {
      return res.badData(data, "Data Banner can't be load", reply);
    }

    return res.ok(data, "Data Banner successfully to load", reply);
  } catch (error) {
    console.log("error");
    return Promise.reject(error);
  }
}

async function getAllByValidation(request, reply) {
  const { limit, offset } = request.query;

  let sql = `SELECT * FROM banner WHERE start_date < CURDATE() AND end_date > CURDATE() AND status = 1`;
  if (util.isParamsUndifined(request.query, "limit", "offset")) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }

  let data = await new Promise((resolve) =>
    connection.query(sql, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      if (rows.length > 0) {
        let array = [];
        rows.forEach((element) => {
          array.push({
            id: element.id,
            title: element.title,
            description: element.description,
            action_url: element.action_url,
            start_date: moment(element.start_date)
              .format("YYYY-MM-DD")
              .toString(),
            end_date: moment(element.end_date).format("YYYY-MM-DD").toString(),
            image_url: element.image_url,
            priority: element.priority,
            status: element.status,
          });
        });

        return resolve(array);
      } else {
        return resolve(null);
      }
    })
  );

  if (!data) {
    return res.badData(data, "Data Banner can't be load", reply);
  }

  return res.ok(data, "Data Banner successfully to load", reply);
}

async function getById(request, reply) {
  let id = request.params.id;
  let sql = `SELECT * FROM banner WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.length > 0
        ? resolve({
            id: rows[0].id,
            title: rows[0].title,
            description: rows[0].description,
            mobile_type: rows[0].mobile_type,
            action_url: rows[0].action_url,
            merchant_id: rows[0].merchant_id,
            product_id: rows[0].product_id,
            start_date: moment(rows[0].start_date)
              .format("YYYY-MM-DD")
              .toString(),
            end_date: moment(rows[0].end_date).format("YYYY-MM-DD").toString(),
            image_url: rows[0].image_url,
            priority: rows[0].priority,
            status: rows[0].status,
          })
        : resolve(null);
    })
  );

  if (!data) {
    return res.notFound(data, "Data Banner can't be found", reply);
  }

  return res.ok(data, "Data Banner successfully to load", reply);
}
async function create(request, reply) {
  const {
    id,
    title,
    description,
    mobile_type: mobileType,
    action_url: actionUrl,
    merchant_id: merchantId,
    product_id: productId,
    start_date: startDate,
    end_date: endDate,
    image_url: imageUrl,
    priority,
    status,
  } = request.body;

  let sql = `INSERT INTO banner (id, title, description,mobile_type, action_url, merchant_id, product_id, start_date, end_date, image_url, priority, status) values(?,?,?,?,?,?,?,?,?,?,?,?)`;

  let data = await new Promise((resolve) =>
    connection.query(
      sql,
      [
        id,
        title,
        description,
        mobileType,
        actionUrl,
        merchantId,
        productId,
        startDate,
        endDate,
        imageUrl,
        priority,
        status,
      ],
      function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest(false, `${error}`, reply);
        }

        return rows.affectedRows > 0 ? resolve(true) : resolve(false);
      }
    )
  );

  if (!data) {
    return res.badData(data, "Data Banner can't add to table", reply);
  }

  return res.ok(data, "Data Banner added to table successfully", reply);
}

async function update(request, reply) {
  const id = request.params.id;
  const now = new Date();
  let queryBuilder = "";
  let arrValue = [];
  const banner = request.body;
  await Object.keys(banner).forEach(function (key) {
    queryBuilder = " " + queryBuilder + key + " = ? ,";
    arrValue.push(banner[key]);
  });
  let sql =
    "UPDATE banner SET " + queryBuilder + "updated_date = ? where id = ?";
  arrValue.push(now);
  arrValue.push(id);
  let data = await new Promise((resolve) =>
    connection.query(sql, arrValue, function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Banner can't update to table", reply);
  }

  return res.ok(data, "Data Banner updated to table successfully", reply);
}

async function sequence(request, reply) {
  const banner = request.body;
  let queryParams = [];
  for (var i = 0; i < banner.length; i++) {
    const banners = banner[i];
    let id = banners.id;
    let priority = banners.priority;
    let param = [id, priority];
    queryParams.push(param);
  }

  console.log(queryParams);

  let sql = `INSERT into banner (id, priority)
            VALUES ?
            ON DUPLICATE KEY UPDATE
            priority = VALUES(priority)`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [queryParams], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Banner can't update to table", reply);
  }

  return res.ok(data, "Data Banner updated to table successfully", reply);
}

//update status

async function updateStatus(request, reply) {
  const { id, status } = request.body;

  let sql = `UPDATE banner SET status =?  WHERE id =?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [status, id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Banner can't update to table", reply);
  }

  return res.ok(data, "Data Banner updated to table successfully", reply);
}

async function destroy(request, reply) {
  let id = request.params.id;
  let sql = `DELETE FROM banner WHERE id = ?`;

  let data = await new Promise((resolve) =>
    connection.query(sql, [id], function (error, rows) {
      if (error) {
        console.log(error);
        return res.badRequest("", `${error}`, reply);
      }

      return rows.affectedRows > 0 ? resolve(true) : resolve(false);
    })
  );

  if (!data) {
    return res.badData(data, "Data Banner can't delete from table", reply);
  }

  return res.ok(data, "Data Banner deleted from table successfully", reply);
}
module.exports = {
  getAll,
  getAllByValidation,
  getById,
  create,
  update,
  destroy,
  updateStatus,
  sequence,
};
