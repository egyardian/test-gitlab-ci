let res = require("../response");
let connection = require("../plugins/mysqlConnection");
let moment = require("moment");
const util = require("../util");

async function getAll(request, reply) {
  const { limit, offset } = request.query;

  let sql = `SELECT d.*, u.first_name, u.last_name  FROM deposit as d inner join users as u on u.uid = d.user_id ORDER BY d.id DESC`;
  if (util.isParamsUndifined(request.query, "limit", "offset")) {
    sql += ` LIMIT ${limit} OFFSET ${offset}`;
  }

  try {
    let data = await new Promise((resolve) =>
      connection.query(sql, function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest("", `${error}`, reply);
        }

        if (rows.length > 0) {
          let array = [];
          rows.forEach((element) => {
            array.push({
              id: element.id,
              type: element.type,
              user_id: element.user_id,
              first_name: element.first_name,
              last_name: element.last_name,
              nominal: element.nominal,
              note: element.note,
              payment_status: element.payment_status,
              verify_status: element.verify_status,
              created_date: moment(element.created_date)
                .format("YYYY-MM-DD")
                .toString(),
            });
          });

          return resolve(array);
        } else {
          return resolve(null);
        }
      })
    );

    if (!data) {
      return res.badData(data, "Data deposit can't be load", reply);
    }

    return res.ok(data, "Data deposit successfully to load", reply);
  } catch (error) {
    console.log("error");
    return Promise.reject(error);
  }
}

async function create(request, reply) {
  const {
    type,
    user_id: userId,
    nominal,
    note,
    payment_status: paymentStatus,
    verify_status: verifyStatus,
  } = request.body;

  const now = new Date();

  let sql = `INSERT INTO deposit (type, user_id, nominal, note,created_date, payment_status, verify_status) values(?,?,?,?,?,?,?)`;

  let data = await new Promise((resolve) =>
    connection.query(
      sql,
      [type, userId, nominal, note, now, paymentStatus, verifyStatus],
      function (error, rows) {
        if (error) {
          console.log(error);
          return res.badRequest(false, `${error}`, reply);
        }

        return rows.affectedRows > 0 ? resolve(true) : resolve(false);
      }
    )
  );

  if (!data) {
    return res.badData(data, "Data Deposit can't add to table", reply);
  }

  return res.ok(data, "Data Deposit added to table successfully", reply);
}

module.exports = {
  getAll,
  create,
};
