const fastify = require("fastify");
const isDocker = require("is-docker");
const cors = require('fastify-cors')
const { APP_PORT } = require("./environment");

const whitelist = [`http://127.0.0.1:3000`, `http://180.250.247.43:4001`, `http://api.kulego.id`];
const corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
  credentials: true,
  methods: ['GET', 'PUT', 'POST', 'DELETE']
};

const app = fastify({
  bodyLimit: 1048576 * 2,
  logger: {
    prettyPrint: true,
  },
});

app.register(require('fastify-cors'), { 
  corsOptions
})

app.get("/", async (request, reply) => {
  return {
    hello: "world",
  };
});

app.register(require("./routes/api"), {
  prefix: "api",
});

const start = async () => {
  try {
    if (isDocker()) {
      await app.listen(APP_PORT, "0.0.0.0");
    } else {
      await app.listen(APP_PORT);
    }
    app.log.info(`server listening on ${app.server.address().port}`);
  } catch (err) {
    app.log.error(err);
    process.exit(1);
  }
};
start();
