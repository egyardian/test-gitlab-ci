exports.isParamsUndifined = (obj, ...args) => {
  for (let key in args){
    if (!Object.keys(obj).includes(args[key])) {
        return false
    }
  }
  return true
};

exports.isParamsNull = (obj, ...args) => {
  for (let key in args){
    if (obj[args[key]] != null && obj[args[key]] != '') {  
      return false
    }
  }
  return true
};

exports.insertString = (main_string, ins_string, pos) => {
  if (typeof pos == "undefined") {
    pos = 0;
  }
  if (typeof ins_string == "undefined") {
    ins_string = "";
  }
  return main_string.slice(0, pos) + ins_string + main_string.slice(pos);
};

/**
 * Merge object that has same parentId
 * @param {array} array array of object
 * @param {int} childrenKey amount of parentId that has children
 **/
exports.mergeObject = (array, childrenKey) => {
  // get all parent key from object except children key
  let arrayKeys = Object.keys(array[0]).splice(
    0,
    Object.keys(array[0]).length - childrenKey
  );

  let hash = Object.create(null);
  let dataResult = [];

  array.forEach((value) => {
    let key = arrayKeys
      .map((element) => {
        return value[element];
      })
      .join("|");
    if (!hash[key]) {
      hash[key] = {};
      arrayKeys.forEach((element) => {
        hash[key][element] = value[element];
      });
      hash[key].schedules = [];
      dataResult.push(hash[key]);
    }

    hash[key].schedules = hash[key].schedules.concat(value.schedules);
  });
  return dataResult;
};

exports.camelToSnakeCase = (str) => {
  return str.replace(/[A-Z]/g, letter => `_${letter.toLowerCase()}`);
}