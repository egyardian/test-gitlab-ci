Kulego Service

Prerequisites

Node.js | https://nodejs.org/en/

Fastify Js | https://www.fastify.io/

How to use
Clone this project and install dependencies:
$ git clone https://gitlab.com/kulego-id/kulego-service-mvp2.git
$ cd kulego-service-mvp2
\$ npm install

Project Structure
.
├── src/
| └── plugins/
| | └── sample.js _ Plugin definition
| ├── routes/
│ │ ├── api/
│ │ | └── index.js _ REST routes
│ | └──docs/
| | └── index.js _ API REST routes doc
| ├── services/
| | └── sample.js _ Sample service
| ├── app.js _ Server definition
| ├── environment.js _ Environment definition
| └── response.js _ Return response definition
├── test/
| └── sample.spec.js _ Unit test service definition
└── package.json
